# VentilAid CPAP Control Panel

This is control panel for [VentilAid](https://www.ventilaid.org/) CPAP device. It implements communication protocol defined [here](https://drive.google.com/file/d/1IZ-owo7fDrHt-_xx8rP0xzdJj9mLZ40c/view).

### Usage

We provide [AppImage](https://appimage.org/) for Linux platform. Simply go to [latest pipeline for master branch](https://gitlab.com/Urbicum/ventilaidcpapcontrolpanel/pipelines/latest), select `Jobs` tab and download artifact for `build` job.
For now for Windows and Mac platforms application has to be built manually.

Use `--headless` to run application without UI. This can be useful for logging messages faster than 10Hz.

### Emulating VentilAid CPAP device

For testing UI without VentilAid CPAP device there are three options:
* if you own an Arduino you can use [util/serial_protocol_emulator/serial_protocol_emulator.ino](util/serial_protocol_emulator/serial_protocol_emulator.ino)
* [fakeDataSender.sh](util/fakeDataSender.sh) script - regularly sends a Status Message (v1) or a Main Params Mesage (v2)
* [emul.cpp](util/emul.cpp) program emulates some of the VentilAid device behaviours (v2), i.e.:
    * sending Main Params Message regularly
    * receiving Breathalizer Params and Medical Settings Params to update proper variables
    * receiving Request Message and sending Breathalizer Params or Medical Settings Params in reply

### Verify V2 messages
If there are changes to how CPAP device receives messages, you can check if they all still work using:
* [util/testV2Messages.sh](util/testV2Messages.sh) to send all currently supported messages in V2 api.
* usage similar to [fakeDataSender.sh](util/fakeDataSender.sh), except this verifier only sends one of each messages instead of running continously.

Remember to run application with `--skipCrc` command line argument when using emulators. Currently emulators leave Crc checksum empty and it would lead to all messages being rejected by application.
