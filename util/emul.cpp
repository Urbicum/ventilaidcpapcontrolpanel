#include <iostream>
#include <map>
#include <math.h>
#include <poll.h>
#include <unistd.h>
#include <vector>

// This program replicates some behaviours of a VentilAid device.
// The goal was to allow one to test the control panel
// without connecting to a real VA device.
//
// Functionality:
// - sending Main Params message every 0.1 second
// - receiving Medical Settings Params message and updating proper values
// - receiving Breathalizer Params message and updating proper values
// - receiving Request Message message
//   and sending back Medical Settings Params or Breathalyser Params message
//
// This file can be compiled using:
// g++ emul.cpp -o emul -std=c++11
//
// Before running this program run socat to create connected pseudo-terminals
// that will pretend to be a serial connection:
// socat -d -d pty,raw,echo=0 pty,raw,echo=0
//
// This program requires specifying a pseudo-terminal to which it will listen
// and which will be used to send messages. Example:
// ./emul /dev/pts/2

static constexpr int ITERATIONS = 10000;
static constexpr int INTERVAL = 100; // miliseconds
static constexpr int BUFFER_SIZE = 1024; // bytes
static constexpr bool SEND_ALARMS = true;
static constexpr bool SEND_RANDOM_ALARMS = false;

static constexpr float PI = 3.14159265358979f;
static constexpr float TWO_PI = 2.0f * PI;

static const uint8_t VA_FIRST_BYTE = 0xf0;
static const uint8_t VA_SECOND_BYTE = 0x0f;
static const uint8_t VA_LAST_BYTE = 0xaa;

// Messages from device to a computer
static const uint8_t VA_DVC_MSG_ID_MAIN_PARAMS = 0x01; // 1
static const uint8_t VA_DVC_MSG_ID_MEDICAL_SETTINGS_PARAMS = 0x02; // 2
static const uint8_t VA_DVC_MSG_ID_PID_SETTINGS_PARAMS = 0x03; // 3
static const uint8_t VA_DVC_MSG_ID_FLOW_CALIBRATION_SETTINGS_PARAMS = 0x04; // 4
static const uint8_t VA_DVC_MSG_ID_WORKPOINT_CALIBRATION_SETTINGS_PARAMS = 0x05; // 5
static const uint8_t VA_DVC_MSG_ID_BREATHALYSER_PARAMS = 0x06; // 6
static const uint8_t VA_DVC_MSG_ID_ALARM_STATUS = 0x0a; // 10
static const uint8_t VA_DVC_MSG_ID_ALARM_DETAILS = 0x0b; // 11
static const uint8_t VA_DVC_MSG_ID_PRESSURE_S0_CALIBRATION_SETTINGS_PARAMS = 0x14; // 20
static const uint8_t VA_DVC_MSG_ID_PRESSURE_S1_CALIBRATION_SETTINGS_PARAMS = 0x15; // 21
static const uint8_t VA_DVC_MSG_ID_PRESSURE_S2_CALIBRATION_SETTINGS_PARAMS = 0x16; // 22
static const uint8_t VA_DVC_MSG_ID_PRESSURE_S3_CALIBRATION_SETTINGS_PARAMS = 0x17; // 23
static const uint8_t VA_DVC_MSG_ID_ALARM_THRESHOLD_CURRENT_VALUE = 0x1e; // 30

// Messages from computer to a device
static const uint8_t VA_CMP_MSG_ID_REQUEST_MESSAGE = 0x64; // 100
static const uint8_t VA_CMP_MSG_ID_REQUEST_ALARM_THRESHOLD_MESSAGE = 0x65; // 103
static const uint8_t VA_CMP_MSG_ID_MEDICAL_SETTINGS_PARAMS = 0x66; // 102
static const uint8_t VA_CMP_MSG_ID_PID_SETTINGS_PARAMS = 0x67; // 103
static const uint8_t VA_CMP_MSG_ID_FLOW_CALIBRATION_SETTINGS_PARAMS = 0x68; // 104
static const uint8_t VA_CMP_MSG_ID_WORKPOINT_CALIBRATION_SETTINGS_PARAMS = 0x69; // 105
static const uint8_t VA_CMP_MSG_ID_BREATHALYSER_PARAMS = 0x6a; // 106
static const uint8_t VA_CMP_MSG_ID_RESET_ALARM = 0x6e; // 110
static const uint8_t VA_CMP_MSG_ID_PRESSURE_S0_CALIBRATION_SETTINGS_PARAMS = 0x78; // 120
static const uint8_t VA_CMP_MSG_ID_PRESSURE_S1_CALIBRATION_SETTINGS_PARAMS = 0x79; // 121
static const uint8_t VA_CMP_MSG_ID_PRESSURE_S2_CALIBRATION_SETTINGS_PARAMS = 0x7a; // 122
static const uint8_t VA_CMP_MSG_ID_PRESSURE_S3_CALIBRATION_SETTINGS_PARAMS = 0x7b; // 123
static const uint8_t VA_CMP_MSG_ID_SET_ALARM_THRESHOLD_MESSAGE = 0x82; // 130

static const size_t MAIN_PARAMS_MESSAGE_SIZE = 49;
static const size_t MEDICAL_SETTINGS_PARAMS_MESSAGE_SIZE = 32;
static const size_t PID_SETTINGS_PARAMS_MESSAGE_SIZE = 67;
static const size_t FLOW_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE = 27;
static const size_t WORKPOINT_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE = 27;
static const size_t PRESSURE_S0_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE = 23;
static const size_t PRESSURE_S1_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE = 23;
static const size_t PRESSURE_S2_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE = 23;
static const size_t PRESSURE_S3_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE = 23;
static const size_t BREATHALYZER_PARAMS_MESSAGE_SIZE = 23;
static const size_t REQUEST_MESSAGE_MESSAGE_SIZE = 8;
static const size_t REQUEST_ALARM_THRESHOLD_MESSAGE_MESSAGE_SIZE = 9;
static const size_t ALARM_STATUS_MESSAGE_SIZE = 11;
static const size_t ALARM_DETAILS_MESSAGE_SIZE = 15;
static const size_t ALARM_THRESHOLD_MESSAGE_SIZE = 25;
static const size_t RESET_ALARM_MESSAGE_SIZE = 11;

static const uint8_t Mode_Idle = 0;
static const uint8_t Mode_CPAP = 1;
static const uint8_t Mode_BiPAP = 2;
static const uint8_t Mode_SensorlessBiPAP = 3;

static struct MedicalSettings {
    float PEEP;
    float targetFiO2;
    float PIP;
    float IERatio;
    float breathPerMin;
    float targetVolume;
    uint8_t mode;
} medicalSettings{ 5.0f, 21.0f, 10.0f, 3.0f, 12.0f, 1000.0f, Mode_CPAP };

static uint8_t medicalSettingsParamsMessage[MEDICAL_SETTINGS_PARAMS_MESSAGE_SIZE] = {
    VA_FIRST_BYTE,
    VA_SECOND_BYTE,
    VA_DVC_MSG_ID_MEDICAL_SETTINGS_PARAMS,
    static_cast<uint8_t>(MEDICAL_SETTINGS_PARAMS_MESSAGE_SIZE),
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0,
    0, 0,
    VA_LAST_BYTE
};

static struct PIDSettings {
    float MOTPID_P;
    float MOTPID_I;
    float MOTPID_D;
    float MOTPID_MAX;
    float MOTPID_MIN;
    float PRESPID_P;
    float PRESPID_I;
    float PRESPID_D;
    float PRESPID_MAX;
    float PRESPID_MIN;
    float PEEPPID_P;
    float PEEPPID_I;
    float PEEPPID_D;
    float PEEPPID_MAX;
    float PEEPPID_MIN;
} pidSettings{ 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f, 15.0f };

static uint8_t pidSettingsParamsMessage[PID_SETTINGS_PARAMS_MESSAGE_SIZE] = {
    VA_FIRST_BYTE,
    VA_SECOND_BYTE,
    VA_DVC_MSG_ID_PID_SETTINGS_PARAMS,
    static_cast<uint8_t>(PID_SETTINGS_PARAMS_MESSAGE_SIZE),
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0,
    VA_LAST_BYTE
};

static struct FlowCalibrationSettings {
    float C0;
    float C1;
    float C2;
    float C3;
    float C4;
} flowCalibrationSettings{ 1.0f, 2.0f, 3.0f, 4.0f, 5.0f };

static uint8_t flowCalibrationSettingsParamsMessage[FLOW_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE] = {
    VA_FIRST_BYTE,
    VA_SECOND_BYTE,
    VA_DVC_MSG_ID_FLOW_CALIBRATION_SETTINGS_PARAMS,
    static_cast<uint8_t>(FLOW_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE),
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0,
    VA_LAST_BYTE
};

static struct WorkpointCalibrationSettings {
    float C0;
    float C1;
    float C2;
    float C3;
    float C4;
} workpointCalibrationSettings{ 1.0f, 2.0f, 3.0f, 4.0f, 5.0f };

static uint8_t workpointCalibrationSettingsParamsMessage[WORKPOINT_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE] = {
    VA_FIRST_BYTE,
    VA_SECOND_BYTE,
    VA_DVC_MSG_ID_WORKPOINT_CALIBRATION_SETTINGS_PARAMS,
    static_cast<uint8_t>(WORKPOINT_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE),
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0,
    VA_LAST_BYTE
};

static struct PressureS0CalibrationSettings {
    float C0;
    float C1;
    float C2;
    float C3;
} pressureS0CalibrationSettings{ 1.0f, 2.0f, 3.0f, 4.0f };

static uint8_t pressureS0CalibrationSettingsParamsMessage[PRESSURE_S0_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE] = {
    VA_FIRST_BYTE,
    VA_SECOND_BYTE,
    VA_DVC_MSG_ID_PRESSURE_S0_CALIBRATION_SETTINGS_PARAMS,
    static_cast<uint8_t>(PRESSURE_S0_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE),
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0,
    VA_LAST_BYTE
};

static struct PressureS1CalibrationSettings {
    float C0;
    float C1;
    float C2;
    float C3;
} pressureS1CalibrationSettings{ 1.0f, 2.0f, 3.0f, 4.0f };

static uint8_t pressureS1CalibrationSettingsParamsMessage[PRESSURE_S1_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE] = {
    VA_FIRST_BYTE,
    VA_SECOND_BYTE,
    VA_DVC_MSG_ID_PRESSURE_S1_CALIBRATION_SETTINGS_PARAMS,
    static_cast<uint8_t>(PRESSURE_S1_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE),
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0,
    VA_LAST_BYTE
};

static struct PressureS2CalibrationSettings {
    float C0;
    float C1;
    float C2;
    float C3;
} pressureS2CalibrationSettings{ 1.0f, 2.0f, 3.0f, 4.0f };

static uint8_t pressureS2CalibrationSettingsParamsMessage[PRESSURE_S2_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE] = {
    VA_FIRST_BYTE,
    VA_SECOND_BYTE,
    VA_DVC_MSG_ID_PRESSURE_S2_CALIBRATION_SETTINGS_PARAMS,
    static_cast<uint8_t>(PRESSURE_S2_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE),
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0,
    VA_LAST_BYTE
};

static struct PressureS3CalibrationSettings {
    float C0;
    float C1;
    float C2;
    float C3;
} pressureS3CalibrationSettings{ 1.0f, 2.0f, 3.0f, 4.0f };

static uint8_t pressureS3CalibrationSettingsParamsMessage[PRESSURE_S3_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE] = {
    VA_FIRST_BYTE,
    VA_SECOND_BYTE,
    VA_DVC_MSG_ID_PRESSURE_S3_CALIBRATION_SETTINGS_PARAMS,
    static_cast<uint8_t>(PRESSURE_S3_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE),
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0,
    VA_LAST_BYTE
};

static struct BreathalyzerParams {
    float ptrig;
    float vtrig;
    float esens;
    float intsens;
} breathalyzerParams{ 1.5f, 5.0f, 50.0f, 50.0f };

static uint8_t breathalyzerParamsMessage[BREATHALYZER_PARAMS_MESSAGE_SIZE] = {
    VA_FIRST_BYTE,
    VA_SECOND_BYTE,
    VA_DVC_MSG_ID_BREATHALYSER_PARAMS,
    static_cast<uint8_t>(BREATHALYZER_PARAMS_MESSAGE_SIZE),
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0,
    VA_LAST_BYTE
};

static struct MainParams {
    float pressure;
    float flow;
    float inhalePressureMean;
    float exhalePressureMean;
    float inhaleVolume;
    float inhaleVolumePerMin;
    float exhaleVolume;
    float breathPerMin;
    float lungCompliance;
    float rotameterTarget;
    uint8_t lastInhaleType;
    uint8_t breathPhase;
} mainParams{ 11.0f, 0.0f, 7.5f, 7.5f, 1000.0f, 30000.0f, 1000.0f, 50.0f, 20.0f, 10.0f, 0, 0 };

static uint8_t mainParamsMessage[MAIN_PARAMS_MESSAGE_SIZE] = {
    VA_FIRST_BYTE,
    VA_SECOND_BYTE,
    VA_DVC_MSG_ID_MAIN_PARAMS,
    static_cast<uint8_t>(MAIN_PARAMS_MESSAGE_SIZE),
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0,
    0,
    0, 0,
    VA_LAST_BYTE
};

static struct OtherParams {
    float powerSupplyVoltage;
    float batteryVoltage;
    //float oxygenPressure;
    //float oxygenConcentration;
    //float airPressure;
} otherParams{ 220.0f, 12.0f };

static uint8_t alarmStatusMessage[ALARM_STATUS_MESSAGE_SIZE] = {
    VA_FIRST_BYTE,
    VA_SECOND_BYTE,
    VA_DVC_MSG_ID_ALARM_STATUS,
    static_cast<uint8_t>(ALARM_STATUS_MESSAGE_SIZE),
    0, 0, 0, 0,
    0, 0,
    VA_LAST_BYTE
};

static uint8_t alarmDetailsMessage[ALARM_DETAILS_MESSAGE_SIZE] = {
    VA_FIRST_BYTE,
    VA_SECOND_BYTE,
    VA_DVC_MSG_ID_ALARM_DETAILS,
    static_cast<uint8_t>(ALARM_DETAILS_MESSAGE_SIZE),
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0,
    VA_LAST_BYTE
};

static uint8_t alarmThresholdCurrentValueMessage[ALARM_THRESHOLD_MESSAGE_SIZE] = {
    VA_FIRST_BYTE,
    VA_SECOND_BYTE,
    VA_DVC_MSG_ID_ALARM_THRESHOLD_CURRENT_VALUE,
    static_cast<uint8_t>(ALARM_THRESHOLD_MESSAGE_SIZE),
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0,
    0,
    0, 0,
    VA_LAST_BYTE
};

typedef std::pair<int, int> AlarmId;
static constexpr AlarmId ALARM_ID_POWER_SUPPLY_NO_VOLTAGE = { 3, 1 };
static constexpr AlarmId ALARM_ID_POWER_SUPPLY_LOW_VOLTAGE = { 3, 2 };
static constexpr AlarmId ALARM_ID_POWER_SUPPLY_HIGH_VOLTAGE = { 3, 3 };
static constexpr AlarmId ALARM_ID_BATTERY_SUPPLY_NO_BATTERY = { 4, 1 };
static constexpr AlarmId ALARM_ID_BATTERY_SUPPLY_LOW_VOLTAGE = { 4, 2 };
static constexpr AlarmId ALARM_ID_BATTERY_SUPPLY_HIGH_VOLTAGE = { 4, 3 };
static constexpr AlarmId ALARM_ID_OXYGEN_LOW_PRESSURE = { 5, 1 };
static constexpr AlarmId ALARM_ID_OXYGEN_HIGH_PRESSURE = { 5, 2 };
static constexpr AlarmId ALARM_ID_OXYGEN_LOW_CONCENTRATION = { 5, 3 };
static constexpr AlarmId ALARM_ID_OXYGEN_HIGH_CONCENTRATION = { 5, 4 };
static constexpr AlarmId ALARM_ID_AIR_PRESSURE_LOW_PRESSURE = { 6, 1 };
static constexpr AlarmId ALARM_ID_AIR_PRESSURE_HIGH_PRESSURE = { 6, 2 };
static constexpr AlarmId ALARM_ID_AIR_VOLUME_MINUTE_VOLUME_LOW = { 7, 1 };
static constexpr AlarmId ALARM_ID_AIR_VOLUME_MINUTE_VOLUME_HIGH = { 7, 2 };
static constexpr AlarmId ALARM_ID_AIR_VOLUME_BREATH_VOLUME_LOW = { 7, 3 };
static constexpr AlarmId ALARM_ID_AIR_VOLUME_BREATH_VOLUME_HIGH = { 7, 4 };
static constexpr AlarmId ALARM_ID_HOSES_LEAK = { 8, 1 };
static constexpr AlarmId ALARM_ID_HOSES_DISCONNECTED = { 8, 2 };
static constexpr AlarmId ALARM_ID_BREATHS_PER_MIN_NO_BREATHS = { 9, 1 };
static constexpr AlarmId ALARM_ID_BREATHS_PER_MIN_LOW_BPM = { 9, 2 };
static constexpr AlarmId ALARM_ID_BREATHS_PER_MIN_HIGH_BPM = { 9, 3 };
static constexpr AlarmId ALARM_ID_DEVICE_MOTOR_ALARM = { 10, 1 };
static constexpr AlarmId ALARM_ID_DEVICE_PRESSURE_SENSOR_ALARM = { 10, 2 };
static constexpr AlarmId ALARM_ID_DEVICE_USER_PANEL_ALARM = { 10, 3 };

static std::map<AlarmId, bool> activeAlarmsMap = {
    { ALARM_ID_POWER_SUPPLY_NO_VOLTAGE, false },
    { ALARM_ID_POWER_SUPPLY_LOW_VOLTAGE, false },
    { ALARM_ID_POWER_SUPPLY_HIGH_VOLTAGE, false },
    { ALARM_ID_BATTERY_SUPPLY_NO_BATTERY, false },
    { ALARM_ID_BATTERY_SUPPLY_LOW_VOLTAGE, false },
    { ALARM_ID_BATTERY_SUPPLY_HIGH_VOLTAGE, false },
    { ALARM_ID_OXYGEN_LOW_PRESSURE, false },
    { ALARM_ID_OXYGEN_HIGH_PRESSURE, false },
    { ALARM_ID_OXYGEN_LOW_CONCENTRATION, false },
    { ALARM_ID_OXYGEN_HIGH_CONCENTRATION, false },
    { ALARM_ID_AIR_PRESSURE_LOW_PRESSURE, false },
    { ALARM_ID_AIR_PRESSURE_HIGH_PRESSURE, false },
    { ALARM_ID_AIR_VOLUME_MINUTE_VOLUME_LOW, false },
    { ALARM_ID_AIR_VOLUME_MINUTE_VOLUME_HIGH, false },
    { ALARM_ID_AIR_VOLUME_BREATH_VOLUME_LOW, false },
    { ALARM_ID_AIR_VOLUME_BREATH_VOLUME_HIGH, false },
    { ALARM_ID_HOSES_LEAK, false },
    { ALARM_ID_HOSES_DISCONNECTED, false },
    { ALARM_ID_BREATHS_PER_MIN_NO_BREATHS, false },
    { ALARM_ID_BREATHS_PER_MIN_LOW_BPM, false },
    { ALARM_ID_BREATHS_PER_MIN_HIGH_BPM, false },
    { ALARM_ID_DEVICE_MOTOR_ALARM, false },
    { ALARM_ID_DEVICE_PRESSURE_SENSOR_ALARM, false },
    { ALARM_ID_DEVICE_USER_PANEL_ALARM, false }
};
static bool activeAlarmsMapChanged = false;

// TODO: assign reasonable values
static std::map<AlarmId, float> alarmThresholds = {
    { ALARM_ID_POWER_SUPPLY_LOW_VOLTAGE, 190.0f },
    { ALARM_ID_POWER_SUPPLY_HIGH_VOLTAGE, 260.0f },
    { ALARM_ID_BATTERY_SUPPLY_LOW_VOLTAGE, 10.0f },
    { ALARM_ID_BATTERY_SUPPLY_HIGH_VOLTAGE, 18.0f },
    { ALARM_ID_OXYGEN_LOW_PRESSURE, 1.0f },
    { ALARM_ID_OXYGEN_HIGH_PRESSURE, 10.0f },
    { ALARM_ID_OXYGEN_LOW_CONCENTRATION, 10.0f },
    { ALARM_ID_OXYGEN_HIGH_CONCENTRATION, 50.0f },
    { ALARM_ID_AIR_PRESSURE_LOW_PRESSURE, 1.0f },
    { ALARM_ID_AIR_PRESSURE_HIGH_PRESSURE, 10.0f },
    { ALARM_ID_AIR_VOLUME_MINUTE_VOLUME_LOW, 1800.0f },
    { ALARM_ID_AIR_VOLUME_MINUTE_VOLUME_HIGH, 51000.0f },
    { ALARM_ID_AIR_VOLUME_BREATH_VOLUME_LOW, 300.0f },
    { ALARM_ID_AIR_VOLUME_BREATH_VOLUME_HIGH, 1700.0f },
    { ALARM_ID_BREATHS_PER_MIN_LOW_BPM, 6.0f },
    { ALARM_ID_BREATHS_PER_MIN_HIGH_BPM, 30.0f }
};

void updateActiveAlarmsMap()
{
    auto setAlarm = [](AlarmId alarmId, bool active) {
        if (activeAlarmsMap[alarmId] != active) {
            activeAlarmsMap[alarmId] = active;
            activeAlarmsMapChanged = true;
        }
    };

    auto checkValue = [&setAlarm](float value, AlarmId lowId, AlarmId highId) {
        if (value <= alarmThresholds[lowId]) {
            setAlarm(lowId, true);
            setAlarm(highId, false);
        } else if (value >= alarmThresholds[highId]) {
            setAlarm(lowId, false);
            setAlarm(highId, true);
        } else {
            setAlarm(lowId, false);
            setAlarm(highId, false);
        }
    };

    checkValue(otherParams.powerSupplyVoltage, ALARM_ID_POWER_SUPPLY_LOW_VOLTAGE, ALARM_ID_POWER_SUPPLY_HIGH_VOLTAGE);
    checkValue(otherParams.batteryVoltage, ALARM_ID_BATTERY_SUPPLY_LOW_VOLTAGE, ALARM_ID_BATTERY_SUPPLY_HIGH_VOLTAGE);
    // TODO: Handle missing alarms
    checkValue(mainParams.inhaleVolume, ALARM_ID_AIR_VOLUME_BREATH_VOLUME_LOW, ALARM_ID_AIR_VOLUME_BREATH_VOLUME_HIGH);
    checkValue(mainParams.inhaleVolumePerMin, ALARM_ID_AIR_VOLUME_MINUTE_VOLUME_LOW, ALARM_ID_AIR_VOLUME_MINUTE_VOLUME_HIGH);
    checkValue(mainParams.breathPerMin, ALARM_ID_BREATHS_PER_MIN_LOW_BPM, ALARM_ID_BREATHS_PER_MIN_HIGH_BPM);
}

void updateMedicalSettingsParamsMessage()
{
    uint8_t* ptr = medicalSettingsParamsMessage;
    *((float*)(ptr + 4)) = medicalSettings.PEEP;
    *((float*)(ptr + 8)) = medicalSettings.targetFiO2;
    *((float*)(ptr + 12)) = medicalSettings.PIP;
    *((float*)(ptr + 16)) = medicalSettings.IERatio;
    *((float*)(ptr + 20)) = medicalSettings.breathPerMin;
    *((float*)(ptr + 24)) = medicalSettings.targetVolume;
    *(ptr + 28) = medicalSettings.mode;
}

void updatePIDSettingsParamsMessage()
{
    uint8_t* ptr = pidSettingsParamsMessage;
    *((float*)(ptr + 4)) = pidSettings.MOTPID_P;
    *((float*)(ptr + 8)) = pidSettings.MOTPID_I;
    *((float*)(ptr + 12)) = pidSettings.MOTPID_D;
    *((float*)(ptr + 16)) = pidSettings.MOTPID_MAX;
    *((float*)(ptr + 20)) = pidSettings.MOTPID_MIN;
    *((float*)(ptr + 24)) = pidSettings.PRESPID_P;
    *((float*)(ptr + 28)) = pidSettings.PRESPID_I;
    *((float*)(ptr + 32)) = pidSettings.PRESPID_D;
    *((float*)(ptr + 36)) = pidSettings.PRESPID_MAX;
    *((float*)(ptr + 40)) = pidSettings.PRESPID_MIN;
    *((float*)(ptr + 44)) = pidSettings.PEEPPID_P;
    *((float*)(ptr + 48)) = pidSettings.PEEPPID_I;
    *((float*)(ptr + 52)) = pidSettings.PEEPPID_D;
    *((float*)(ptr + 56)) = pidSettings.PEEPPID_MAX;
    *((float*)(ptr + 60)) = pidSettings.PEEPPID_MIN;
}

void updateFlowCalibrationSettingsParamsMessage()
{
    uint8_t* ptr = flowCalibrationSettingsParamsMessage;
    *((float*)(ptr + 4)) = flowCalibrationSettings.C0;
    *((float*)(ptr + 8)) = flowCalibrationSettings.C1;
    *((float*)(ptr + 12)) = flowCalibrationSettings.C2;
    *((float*)(ptr + 16)) = flowCalibrationSettings.C3;
    *((float*)(ptr + 20)) = flowCalibrationSettings.C4;
}

void updateWorkpointCalibrationSettingsParamsMessage()
{
    uint8_t* ptr = workpointCalibrationSettingsParamsMessage;
    *((float*)(ptr + 4)) = workpointCalibrationSettings.C0;
    *((float*)(ptr + 8)) = workpointCalibrationSettings.C1;
    *((float*)(ptr + 12)) = workpointCalibrationSettings.C2;
    *((float*)(ptr + 16)) = workpointCalibrationSettings.C3;
    *((float*)(ptr + 20)) = workpointCalibrationSettings.C4;
}

void updatePressureS0CalibrationSettingsParamsMessage()
{
    uint8_t* ptr = pressureS0CalibrationSettingsParamsMessage;
    *((float*)(ptr + 4)) = pressureS0CalibrationSettings.C0;
    *((float*)(ptr + 8)) = pressureS0CalibrationSettings.C1;
    *((float*)(ptr + 12)) = pressureS0CalibrationSettings.C2;
    *((float*)(ptr + 16)) = pressureS0CalibrationSettings.C3;
}

void updatePressureS1CalibrationSettingsParamsMessage()
{
    uint8_t* ptr = pressureS1CalibrationSettingsParamsMessage;
    *((float*)(ptr + 4)) = pressureS1CalibrationSettings.C0;
    *((float*)(ptr + 8)) = pressureS1CalibrationSettings.C1;
    *((float*)(ptr + 12)) = pressureS1CalibrationSettings.C2;
    *((float*)(ptr + 16)) = pressureS1CalibrationSettings.C3;
}

void updatePressureS2CalibrationSettingsParamsMessage()
{
    uint8_t* ptr = pressureS2CalibrationSettingsParamsMessage;
    *((float*)(ptr + 4)) = pressureS2CalibrationSettings.C0;
    *((float*)(ptr + 8)) = pressureS2CalibrationSettings.C1;
    *((float*)(ptr + 12)) = pressureS2CalibrationSettings.C2;
    *((float*)(ptr + 16)) = pressureS2CalibrationSettings.C3;
}

void updatePressureS3CalibrationSettingsParamsMessage()
{
    uint8_t* ptr = pressureS3CalibrationSettingsParamsMessage;
    *((float*)(ptr + 4)) = pressureS3CalibrationSettings.C0;
    *((float*)(ptr + 8)) = pressureS3CalibrationSettings.C1;
    *((float*)(ptr + 12)) = pressureS3CalibrationSettings.C2;
    *((float*)(ptr + 16)) = pressureS3CalibrationSettings.C3;
}

void updateBreathalyzerParamsMessage()
{
    uint8_t* ptr = breathalyzerParamsMessage;
    *((float*)(ptr + 4)) = breathalyzerParams.ptrig;
    *((float*)(ptr + 8)) = breathalyzerParams.vtrig;
    *((float*)(ptr + 12)) = breathalyzerParams.esens;
    *((float*)(ptr + 16)) = breathalyzerParams.intsens;
}

void updateMainParamsMessage()
{
    uint8_t* ptr = mainParamsMessage;
    *((float*)(ptr + 4)) = mainParams.pressure;
    *((float*)(ptr + 8)) = mainParams.flow;
    *((float*)(ptr + 12)) = mainParams.inhalePressureMean;
    *((float*)(ptr + 16)) = mainParams.exhalePressureMean;
    *((float*)(ptr + 20)) = mainParams.inhaleVolume;
    *((float*)(ptr + 24)) = mainParams.inhaleVolumePerMin;
    *((float*)(ptr + 28)) = mainParams.exhaleVolume;
    *((float*)(ptr + 32)) = mainParams.breathPerMin;
    *((float*)(ptr + 36)) = mainParams.lungCompliance;
    *((float*)(ptr + 40)) = mainParams.rotameterTarget;
    *(ptr + 44) = mainParams.lastInhaleType;
    *(ptr + 45) = mainParams.breathPhase;
}

std::pair<AlarmId, AlarmId> getLoHiAlarmIds(uint8_t alarmNo, uint8_t detailNo)
{
    switch (alarmNo) {
    case ALARM_ID_POWER_SUPPLY_LOW_VOLTAGE.first:
        return { ALARM_ID_POWER_SUPPLY_LOW_VOLTAGE, ALARM_ID_POWER_SUPPLY_HIGH_VOLTAGE };
    case ALARM_ID_BATTERY_SUPPLY_LOW_VOLTAGE.first:
        return { ALARM_ID_BATTERY_SUPPLY_LOW_VOLTAGE, ALARM_ID_BATTERY_SUPPLY_HIGH_VOLTAGE };
    case ALARM_ID_OXYGEN_LOW_PRESSURE.first:
        if (detailNo == 2)
            return { ALARM_ID_OXYGEN_LOW_CONCENTRATION, ALARM_ID_OXYGEN_HIGH_CONCENTRATION };
        else
            return { ALARM_ID_OXYGEN_LOW_PRESSURE, ALARM_ID_OXYGEN_HIGH_PRESSURE };
    case ALARM_ID_AIR_PRESSURE_LOW_PRESSURE.first:
        return { ALARM_ID_AIR_PRESSURE_LOW_PRESSURE, ALARM_ID_AIR_PRESSURE_HIGH_PRESSURE };
    case ALARM_ID_AIR_VOLUME_MINUTE_VOLUME_LOW.first:
        if (detailNo == 2)
            return { ALARM_ID_AIR_VOLUME_BREATH_VOLUME_LOW, ALARM_ID_AIR_VOLUME_BREATH_VOLUME_HIGH };
        else
            return { ALARM_ID_AIR_VOLUME_MINUTE_VOLUME_LOW, ALARM_ID_AIR_VOLUME_MINUTE_VOLUME_HIGH };
    case ALARM_ID_BREATHS_PER_MIN_LOW_BPM.first:
        return { ALARM_ID_BREATHS_PER_MIN_LOW_BPM, ALARM_ID_BREATHS_PER_MIN_HIGH_BPM };
    }
    return { { -1, -1 }, { -1, -1 } };
}

void updateAlarmThresholdCurrentValueMessage(uint8_t alarm, uint8_t detail)
{
    const auto loHiIds = getLoHiAlarmIds(alarm, detail);
    uint8_t* ptr = alarmThresholdCurrentValueMessage;
    *((float*)(ptr + 4)) = alarmThresholds[loHiIds.first];
    *((float*)(ptr + 16)) = alarmThresholds[loHiIds.second];
    *(ptr + 20) = alarm;
    *(ptr + 21) = detail;
}

float calculateNewValueUsingSine(float min, float max, int period, int t)
{
    const float t_f = static_cast<float>(t);
    const float period_f = static_cast<float>(period);
    return 0.5f * (max + min + (max - min) * sinf((t_f / period_f) * TWO_PI));
}

void modifyMainParams(int t)
{
    static constexpr int period = 20;
    mainParams.pressure = calculateNewValueUsingSine(-2.0f, 25.0f, period, t);
    mainParams.flow = calculateNewValueUsingSine(-50.0f, 50.0f, period, t);
    mainParams.inhalePressureMean = calculateNewValueUsingSine(0.0f, 15.0f, period, t);
    mainParams.exhalePressureMean = calculateNewValueUsingSine(0.0f, 15.0f, period, t);
    mainParams.inhaleVolume = calculateNewValueUsingSine(0.0f, 2000.0f, period, t);
    mainParams.inhaleVolumePerMin = calculateNewValueUsingSine(0.0f, 60000.0f, period, t);
    mainParams.exhaleVolume = calculateNewValueUsingSine(0.0f, 2000.0f, period, t);
    mainParams.breathPerMin = calculateNewValueUsingSine(0.0f, 40.0f, period, t);
    mainParams.lungCompliance = calculateNewValueUsingSine(0.0f, 100.0f, period, t);
    mainParams.rotameterTarget = calculateNewValueUsingSine(0.0f, 20.0f, period, t);
    mainParams.lastInhaleType = (t % period) ? mainParams.lastInhaleType : (mainParams.lastInhaleType >= 3) ? 1 : mainParams.lastInhaleType + 1;
    mainParams.breathPhase = (t % period) ? mainParams.breathPhase : (mainParams.breathPhase >= 2) ? 1 : mainParams.breathPhase + 1;
}

bool isValidMessageSize(size_t size)
{
    return size == MAIN_PARAMS_MESSAGE_SIZE
        || size == MEDICAL_SETTINGS_PARAMS_MESSAGE_SIZE
        || size == PID_SETTINGS_PARAMS_MESSAGE_SIZE
        || size == FLOW_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE
        || size == WORKPOINT_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE
        || size == PRESSURE_S0_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE
        || size == PRESSURE_S1_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE
        || size == PRESSURE_S2_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE
        || size == PRESSURE_S3_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE
        || size == BREATHALYZER_PARAMS_MESSAGE_SIZE
        || size == RESET_ALARM_MESSAGE_SIZE
        || size == ALARM_THRESHOLD_MESSAGE_SIZE
        || size == REQUEST_ALARM_THRESHOLD_MESSAGE_MESSAGE_SIZE
        || size == REQUEST_MESSAGE_MESSAGE_SIZE;
}

bool extractVAMessage(uint8_t* msg, size_t msgSize, size_t& extractedMsgSize)
{
    if (msgSize > 3
        && msg[0] == VA_FIRST_BYTE
        && msg[1] == VA_SECOND_BYTE
        && isValidMessageSize(static_cast<uint8_t>(msg[3]))
        && msg[msg[3] - 1] == VA_LAST_BYTE) {
        extractedMsgSize = static_cast<size_t>(msg[3]);
        return true;
    }
    return false;
}

void writeToFile(uint8_t* msg, size_t msgSize, FILE* file)
{
    ::fwrite((const void*)msg, 1, msgSize, file);
    ::fflush(file);
}

void readFromFile(uint8_t* bytes, size_t& bytesRead, FILE* file)
{
    bytesRead = ::read(::fileno(file), bytes, BUFFER_SIZE);
}

bool readFromFileWithTimeout(FILE* file, uint8_t* buf, size_t& size, int mlsecTimeout)
{
    struct pollfd fd = { .fd = ::fileno(file), .events = POLLIN };
    if (::poll(&fd, 1, mlsecTimeout) == 1) {
        readFromFile(buf, size, file);
        return true;
    }
    return false;
}

void sendMessage(uint8_t* msg, size_t msgSize, FILE* targetFile)
{
    if (msgSize == 0)
        return;
    writeToFile(msg, msgSize, targetFile);
}

void setRandomAlarm()
{
    int randNum = rand() % activeAlarmsMap.size();
    auto iter = activeAlarmsMap.begin();
    while (randNum > 0) {
        ++iter;
        --randNum;
    }
    if (!iter->second) {
        iter->second = true;
        activeAlarmsMapChanged = true;
    }
}

void sendAlarmStatusMessage(uint32_t alarmsBitmap, FILE* targetFile)
{
    *((uint32_t*)(alarmStatusMessage + 4)) = alarmsBitmap;
    sendMessage(alarmStatusMessage, ALARM_STATUS_MESSAGE_SIZE, targetFile);
}

void sendAlarmDetailsMessage(int alarmId, uint32_t detailBitmap, FILE* targetFile)
{
    *((uint32_t*)(alarmDetailsMessage + 4)) = 1u << alarmId;
    *((uint32_t*)(alarmDetailsMessage + 8)) = detailBitmap;
    sendMessage(alarmDetailsMessage, ALARM_DETAILS_MESSAGE_SIZE, targetFile);
}

void sendAlarmDetailsMessages(FILE* targetFile)
{
    std::vector<AlarmId> alarmsToSend;
    for (auto it = activeAlarmsMap.begin(); it != activeAlarmsMap.end(); ++it)
        if (it->second)
            alarmsToSend.push_back(it->first);

    int alarmId = -1;
    uint32_t detailBitmap = 0;
    for (const auto& a : alarmsToSend) {
        if (a.first != alarmId) {
            if (alarmId >= 0 && detailBitmap)
                sendAlarmDetailsMessage(alarmId, detailBitmap, targetFile);
            detailBitmap = 0u;
            alarmId = a.first;
        }
        detailBitmap |= 1u << a.second;
    }
    if (alarmId >= 0 && detailBitmap)
        sendAlarmDetailsMessage(alarmId, detailBitmap, targetFile);
}

void sendAlarmMessages(FILE* targetFile)
{
    std::vector<AlarmId> alarmsToSend;
    for (auto it = activeAlarmsMap.begin(); it != activeAlarmsMap.end(); ++it)
        if (it->second)
            alarmsToSend.push_back(it->first);

    uint32_t alarmBitmap = 0;
    for (const auto& a : alarmsToSend)
        alarmBitmap |= 1u << a.first;
    sendAlarmStatusMessage(alarmBitmap, targetFile);
    sendAlarmDetailsMessages(targetFile);
}

std::vector<int> whichBitsAreSet(uint32_t bitmap)
{
    std::vector<int> b;
    for (int i = 0; i < 32; ++i)
        if (bitmap & (1u << i))
            b.push_back(i);
    return b;
}

void handleRequestMessageMsg(uint8_t* msg, FILE* targetFile)
{
    const uint8_t requested_msg_id = msg[4];
    uint8_t* message = nullptr;
    size_t messageSize = 0;
    std::cout << "Requested message id: " << static_cast<uint16_t>(requested_msg_id) << std::endl;
    switch (requested_msg_id) {
    case VA_DVC_MSG_ID_MEDICAL_SETTINGS_PARAMS:
        updateMedicalSettingsParamsMessage();
        message = medicalSettingsParamsMessage;
        messageSize = MEDICAL_SETTINGS_PARAMS_MESSAGE_SIZE;
        break;
    case VA_DVC_MSG_ID_PID_SETTINGS_PARAMS:
        updatePIDSettingsParamsMessage();
        message = pidSettingsParamsMessage;
        messageSize = PID_SETTINGS_PARAMS_MESSAGE_SIZE;
        break;
    case VA_DVC_MSG_ID_FLOW_CALIBRATION_SETTINGS_PARAMS:
        updateFlowCalibrationSettingsParamsMessage();
        message = flowCalibrationSettingsParamsMessage;
        messageSize = FLOW_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE;
        break;
    case VA_DVC_MSG_ID_WORKPOINT_CALIBRATION_SETTINGS_PARAMS:
        updateWorkpointCalibrationSettingsParamsMessage();
        message = workpointCalibrationSettingsParamsMessage;
        messageSize = WORKPOINT_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE;
        break;
    case VA_DVC_MSG_ID_PRESSURE_S0_CALIBRATION_SETTINGS_PARAMS:
        updatePressureS0CalibrationSettingsParamsMessage();
        message = pressureS0CalibrationSettingsParamsMessage;
        messageSize = PRESSURE_S0_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE;
        break;
    case VA_DVC_MSG_ID_PRESSURE_S1_CALIBRATION_SETTINGS_PARAMS:
        updatePressureS1CalibrationSettingsParamsMessage();
        message = pressureS1CalibrationSettingsParamsMessage;
        messageSize = PRESSURE_S1_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE;
        break;
    case VA_DVC_MSG_ID_PRESSURE_S2_CALIBRATION_SETTINGS_PARAMS:
        updatePressureS2CalibrationSettingsParamsMessage();
        message = pressureS2CalibrationSettingsParamsMessage;
        messageSize = PRESSURE_S2_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE;
        break;
    case VA_DVC_MSG_ID_PRESSURE_S3_CALIBRATION_SETTINGS_PARAMS:
        updatePressureS3CalibrationSettingsParamsMessage();
        message = pressureS3CalibrationSettingsParamsMessage;
        messageSize = PRESSURE_S3_CALIBRATION_SETTINGS_PARAMS_MESSAGE_SIZE;
        break;
    case VA_DVC_MSG_ID_BREATHALYSER_PARAMS:
        updateBreathalyzerParamsMessage();
        message = breathalyzerParamsMessage;
        messageSize = BREATHALYZER_PARAMS_MESSAGE_SIZE;
        break;
    case VA_DVC_MSG_ID_ALARM_STATUS:
        sendAlarmMessages(targetFile);
        break;
    case VA_DVC_MSG_ID_ALARM_DETAILS:
        sendAlarmDetailsMessages(targetFile);
        break;
    }
    if (message)
        sendMessage(message, messageSize, targetFile);
}

void handleRequestAlarmThresholdMessageMsg(uint8_t* msg, FILE* targetFile)
{
    uint8_t alarmNo = *(msg + 4);
    uint8_t detailNo = *(msg + 5);

    AlarmId lowAlarmId = getLoHiAlarmIds(alarmNo, detailNo).first;
    if (alarmThresholds.find(lowAlarmId) != alarmThresholds.end()) {
        updateAlarmThresholdCurrentValueMessage(alarmNo, detailNo);
        sendMessage(alarmThresholdCurrentValueMessage, ALARM_THRESHOLD_MESSAGE_SIZE, targetFile);
    }
}

void handleMedicalSettingsParamsMsg(uint8_t* msg)
{
    medicalSettings.PEEP = *((float*)(msg + 4));
    medicalSettings.targetFiO2 = *((float*)(msg + 8));
    medicalSettings.PIP = *((float*)(msg + 12));
    medicalSettings.IERatio = *((float*)(msg + 16));
    medicalSettings.breathPerMin = *((float*)(msg + 20));
    medicalSettings.targetVolume = *((float*)(msg + 24));
    medicalSettings.mode = *(msg + 28);
    updateMedicalSettingsParamsMessage();
}

void handlePIDSettingsParamsMsg(uint8_t* msg)
{
    pidSettings.MOTPID_P = *((float*)(msg + 4));
    pidSettings.MOTPID_I = *((float*)(msg + 8));
    pidSettings.MOTPID_D = *((float*)(msg + 12));
    pidSettings.MOTPID_MAX = *((float*)(msg + 16));
    pidSettings.MOTPID_MIN = *((float*)(msg + 20));
    pidSettings.PRESPID_P = *((float*)(msg + 24));
    pidSettings.PRESPID_I = *((float*)(msg + 28));
    pidSettings.PRESPID_D = *((float*)(msg + 32));
    pidSettings.PRESPID_MAX = *((float*)(msg + 36));
    pidSettings.PRESPID_MIN = *((float*)(msg + 40));
    pidSettings.PEEPPID_P = *((float*)(msg + 44));
    pidSettings.PEEPPID_I = *((float*)(msg + 48));
    pidSettings.PEEPPID_D = *((float*)(msg + 52));
    pidSettings.PEEPPID_MAX = *((float*)(msg + 56));
    pidSettings.PEEPPID_MIN = *((float*)(msg + 60));
    updatePIDSettingsParamsMessage();
}

void handleFlowCalibrationSettingsParamsMsg(uint8_t* msg)
{
    flowCalibrationSettings.C0 = *((float*)(msg + 4));
    flowCalibrationSettings.C1 = *((float*)(msg + 8));
    flowCalibrationSettings.C2 = *((float*)(msg + 12));
    flowCalibrationSettings.C3 = *((float*)(msg + 16));
    flowCalibrationSettings.C4 = *((float*)(msg + 20));
    updateFlowCalibrationSettingsParamsMessage();
}

void handleWorkpointCalibrationSettingsParamsMsg(uint8_t* msg)
{
    workpointCalibrationSettings.C0 = *((float*)(msg + 4));
    workpointCalibrationSettings.C1 = *((float*)(msg + 8));
    workpointCalibrationSettings.C2 = *((float*)(msg + 12));
    workpointCalibrationSettings.C3 = *((float*)(msg + 16));
    workpointCalibrationSettings.C4 = *((float*)(msg + 20));
    updateWorkpointCalibrationSettingsParamsMessage();
}

void handlePressureS0CalibrationSettingsParamsMsg(uint8_t* msg)
{
    pressureS0CalibrationSettings.C0 = *((float*)(msg + 4));
    pressureS0CalibrationSettings.C1 = *((float*)(msg + 8));
    pressureS0CalibrationSettings.C2 = *((float*)(msg + 12));
    pressureS0CalibrationSettings.C3 = *((float*)(msg + 16));
    updatePressureS0CalibrationSettingsParamsMessage();
}

void handlePressureS1CalibrationSettingsParamsMsg(uint8_t* msg)
{
    pressureS1CalibrationSettings.C0 = *((float*)(msg + 4));
    pressureS1CalibrationSettings.C1 = *((float*)(msg + 8));
    pressureS1CalibrationSettings.C2 = *((float*)(msg + 12));
    pressureS1CalibrationSettings.C3 = *((float*)(msg + 16));
    updatePressureS1CalibrationSettingsParamsMessage();
}

void handlePressureS2CalibrationSettingsParamsMsg(uint8_t* msg)
{
    pressureS2CalibrationSettings.C0 = *((float*)(msg + 4));
    pressureS2CalibrationSettings.C1 = *((float*)(msg + 8));
    pressureS2CalibrationSettings.C2 = *((float*)(msg + 12));
    pressureS2CalibrationSettings.C3 = *((float*)(msg + 16));
    updatePressureS2CalibrationSettingsParamsMessage();
}

void handlePressureS3CalibrationSettingsParamsMsg(uint8_t* msg)
{
    pressureS3CalibrationSettings.C0 = *((float*)(msg + 4));
    pressureS3CalibrationSettings.C1 = *((float*)(msg + 8));
    pressureS3CalibrationSettings.C2 = *((float*)(msg + 12));
    pressureS3CalibrationSettings.C3 = *((float*)(msg + 16));
    updatePressureS3CalibrationSettingsParamsMessage();
}

void handleBreathalyserParamsMsg(uint8_t* msg)
{
    breathalyzerParams.ptrig = *((float*)(msg + 4));
    breathalyzerParams.vtrig = *((float*)(msg + 8));
    breathalyzerParams.esens = *((float*)(msg + 12));
    breathalyzerParams.intsens = *((float*)(msg + 16));
    updateBreathalyzerParamsMessage();
}

void handleResetAlarmMessage(uint8_t* msg)
{
    const uint32_t codeField = *((uint32_t*)(msg + 4));
    const auto bits = whichBitsAreSet(codeField);
    for (auto itb = bits.begin(); itb != bits.end(); ++itb) {
        for (auto itm = activeAlarmsMap.begin(); itm != activeAlarmsMap.end(); ++itm) {
            if (itm->first.first == *itb && itm->second) {
                itm->second = false;
                activeAlarmsMapChanged = true;
            }
        }
    }
}

void handleSetAlarmThreshold(uint8_t* msg)
{
    float valueLoLo = *((float*)(msg + 4));
    //float valueLo = *((float*)(msg + 8));
    //float valueHi = *((float*)(msg + 12));
    float valueHiHi = *((float*)(msg + 16));
    uint8_t alarmNo = *(msg + 20);
    uint8_t detailNo = *(msg + 21);

    const auto loHiIds = getLoHiAlarmIds(alarmNo, detailNo);

    alarmThresholds[loHiIds.first] = valueLoLo;
    alarmThresholds[loHiIds.second] = valueHiHi;
}

int main(int argc, char** argv)
{
    std::string target = argc > 1 ? argv[1] : "";
    if (target.empty()) {
        std::cout << "You must specify a pseudo-terminal to which you will listen." << std::endl;
        return 1;
    }

    uint8_t bytes[BUFFER_SIZE] = { 0 };
    FILE* targetFile = ::fopen(target.c_str(), "r+");
    if (!targetFile) {
        std::cerr << "Failed to open pseudo-terminal " << target << std::endl;
        return 2;
    }
    std::cout << "Listening to " << target << std::endl;

    for (int i = 0; i < ITERATIONS; ++i) {
        // Send Main Params Message
        modifyMainParams(i);
        updateMainParamsMessage();
        sendMessage(mainParamsMessage, MAIN_PARAMS_MESSAGE_SIZE, targetFile);

        // Send alarm messages if needed
        if (SEND_ALARMS) {
            if (SEND_RANDOM_ALARMS && (i % (10000 / INTERVAL) == 0)) {
                setRandomAlarm();
            } else if (!SEND_RANDOM_ALARMS) {
                updateActiveAlarmsMap();
            }
            if (activeAlarmsMapChanged) {
                sendAlarmMessages(targetFile);
                activeAlarmsMapChanged = false;
            }
        }

        // Read received messages
        size_t bytesRead = 0;
        if (!readFromFileWithTimeout(targetFile, bytes, bytesRead, INTERVAL)) {
            continue;
        }

        size_t currentMsgSize = 0;
        uint8_t* bytesReader = bytes;
        size_t bytesToCheck = bytesRead;
        while (extractVAMessage(bytesReader, bytesToCheck, currentMsgSize)) {
            const uint8_t msg_id = bytesReader[2];
            switch (msg_id) {
            case VA_CMP_MSG_ID_REQUEST_MESSAGE:
                handleRequestMessageMsg(bytesReader, targetFile);
                break;
            case VA_CMP_MSG_ID_REQUEST_ALARM_THRESHOLD_MESSAGE:
                handleRequestAlarmThresholdMessageMsg(bytesReader, targetFile);
                break;
            case VA_CMP_MSG_ID_MEDICAL_SETTINGS_PARAMS:
                handleMedicalSettingsParamsMsg(bytesReader);
                break;
            case VA_CMP_MSG_ID_PID_SETTINGS_PARAMS:
                handlePIDSettingsParamsMsg(bytesReader);
                break;
            case VA_CMP_MSG_ID_FLOW_CALIBRATION_SETTINGS_PARAMS:
                handleFlowCalibrationSettingsParamsMsg(bytesReader);
                break;
            case VA_CMP_MSG_ID_WORKPOINT_CALIBRATION_SETTINGS_PARAMS:
                handleWorkpointCalibrationSettingsParamsMsg(bytesReader);
                break;
            case VA_CMP_MSG_ID_PRESSURE_S0_CALIBRATION_SETTINGS_PARAMS:
                handlePressureS0CalibrationSettingsParamsMsg(bytesReader);
                break;
            case VA_CMP_MSG_ID_PRESSURE_S1_CALIBRATION_SETTINGS_PARAMS:
                handlePressureS1CalibrationSettingsParamsMsg(bytesReader);
                break;
            case VA_CMP_MSG_ID_PRESSURE_S2_CALIBRATION_SETTINGS_PARAMS:
                handlePressureS2CalibrationSettingsParamsMsg(bytesReader);
                break;
            case VA_CMP_MSG_ID_PRESSURE_S3_CALIBRATION_SETTINGS_PARAMS:
                handlePressureS3CalibrationSettingsParamsMsg(bytesReader);
                break;
            case VA_CMP_MSG_ID_BREATHALYSER_PARAMS:
                handleBreathalyserParamsMsg(bytesReader);
                break;
            case VA_CMP_MSG_ID_RESET_ALARM:
                handleResetAlarmMessage(bytesReader);
                break;
            case VA_CMP_MSG_ID_SET_ALARM_THRESHOLD_MESSAGE:
                handleSetAlarmThreshold(bytesReader);
                break;
            }
            bytesReader += currentMsgSize;
            bytesToCheck -= currentMsgSize;
            currentMsgSize = 0;
        }
    }

    return 0;
}
