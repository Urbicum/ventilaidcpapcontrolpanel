#!/bin/bash

# This script sends VentilAid StatusMessage with fake data to the specified virtual serial port.
# It is useful when you don't have a VentilAid device and want to test only the Control Panel.
#
# Before running this script, one must open a fake connection using socat:
#   socat -d -d pty,raw,echo=0 pty,raw,echo=0
# This will display two pseudo-terminals (for example: /dev/pts/1 and /dev/pts/2).
# The Control Panel application must listen to one of them
# and the second one should be passed as an argument to this script.
#
# To make Control Panel listen to the pseudo-terminal (e.g. /dev/pts/2), change:
#     if (QSerialPortInfo::availablePorts().empty()) {
# to:
#     if (QSerialPortInfo::availablePorts().empty() && !QDir().exists("/dev/pts/2")) {
# in MainWindow::openUserSelectedSerial(). And add:
#     ui->portComboBox->addItem("/dev/pts/2");
# in PortSelectionDialog constructor.
#
# Then you can run the script for example like this:
# ./fakeDataSender.sh /dev/pts/1
#
# The Control Panel application should then be able to connect to /dev/pts/2 and receive our fake data.
# Remember to run it with the --skipCrc argument because our fake data have wrong CRC values.
#
# To check if the Control Panel application sends data to the "device", one can simply use:
# cat /dev/pts/1

if [[ -z "$1" ]]; then
  echo "You need to specify a pseudo-terminal to which the data will be sent. E.g.:"
  echo "  $0 /dev/pts/1"
  echo "Available pseudo-terminals:"
  ls /dev/pts/*
  exit 1
fi

version=1         # version of the communication protocol
target=$1         # pseudo-terminal connected with the one to which Control Panel should listen to
interval=0.1      # how long before sending next message (in seconds)
iterations=10000  # how many messages will be sent

echo "I will send fake VentiAid data to $1 every $interval second(s) $iterations times."

# Values in the arrays below will be sent repeatedly.
#
# Version 1:
# floats:
hi_press_vals=(5 6 7 8 9 10 11 12 13 14 15 14 13 12 11 10 9 8 7 6)
lo_press_vals=(5 6 7 8 9 10 11 12 13 14 15 14 13 12 11 10 9 8 7 6)
breaths_vals=(10 14 18 22 26 30 34 37 40 37 34 30 26 22 18 14)
br_prop_vals=(2 3 4 3)
air_vol_vals=(300 350 400 450 500 550 600 550 500 450 400 350)
#
# Version 2:
# floats:
press_vals=(5 6 7 8 9 10 11 12 13 14 15 14 13 12 11 10 9 8 7 6)
flow_vals=(5 6 7 8 9 10 11 12 13 14 15 14 13 12 11 10 9 8 7 6)
ipm_vals=(5 6 7 8 9 10 11 12 13 14 15 14 13 12 11 10 9 8 7 6)
epm_vals=(5 6 7 8 9 10 11 12 13 14 15 14 13 12 11 10 9 8 7 6)
inh_vol_vals=(5 6 7 8 9 10 11 12 13 14 15 14 13 12 11 10 9 8 7 6)
inh_vol_per_min_vals=(5 6 7 8 9 10 11 12 13 14 15 14 13 12 11 10 9 8 7 6)
exh_vol_vals=(5 6 7 8 9 10 11 12 13 14 15 14 13 12 11 10 9 8 7 6)
breaths_per_min_vals=(10 14 18 22 26 30 34 37 40 37 34 30 26 22 18 14)
lung_compl_vals=(5 6 7 8 9 10 11 12 13 14 15 14 13 12 11 10 9 8 7 6)
rot_target_vals=(5 6 7 8 9 10 11 12 13 14 15 14 13 12 11 10 9 8 7 6)
# uint8:
lst_inh_typ_vals=(1 2 3 4 5 4 3 2)
breath_phase_vals=(1 2 3)

# The floats array holds 4 bytes for every float. E.g. the bytes for 4.0f are in floats[4].
# As you can see the list of available conversions is very limited.
# TODO: There must be a better way to do this.
floats=()
floats[0]="\x00\x00\x00\x00"
floats[1]="\x00\x00\x80\x3f"
floats[2]="\x00\x00\x00\x40"
floats[3]="\x00\x00\x40\x40"
floats[4]="\x00\x00\x80\x40"
floats[5]="\x00\x00\xa0\x40"
floats[6]="\x00\x00\xc0\x40"
floats[7]="\x00\x00\xe0\x40"
floats[8]="\x00\x00\x00\x41"
floats[9]="\x00\x00\x10\x41"
floats[10]="\x00\x00\x20\x41"

floats[11]="\x00\x00\x30\x41"
floats[12]="\x00\x00\x40\x41"
floats[13]="\x00\x00\x50\x41"
floats[14]="\x00\x00\x60\x41"
floats[15]="\x00\x00\x70\x41"
floats[16]="\x00\x00\x80\x41"
floats[17]="\x00\x00\x88\x41"
floats[18]="\x00\x00\x90\x41"
floats[19]="\x00\x00\x98\x41"
floats[20]="\x00\x00\xa0\x41"

floats[21]="\x00\x00\xa8\x41"
floats[22]="\x00\x00\xb0\x41"
floats[23]="\x00\x00\xb8\x41"
floats[24]="\x00\x00\xc0\x41"
floats[25]="\x00\x00\xc8\x41"
floats[26]="\x00\x00\xd0\x41"
floats[27]="\x00\x00\xd8\x41"
floats[28]="\x00\x00\xe0\x41"
floats[29]="\x00\x00\xe8\x41"
floats[30]="\x00\x00\xf0\x41"

floats[31]="\x00\x00\xf8\x41"
floats[32]="\x00\x00\x00\x42"
floats[33]="\x00\x00\x04\x42"
floats[34]="\x00\x00\x08\x42"
floats[35]="\x00\x00\x0c\x42"
floats[36]="\x00\x00\x10\x42"
floats[37]="\x00\x00\x14\x42"
floats[38]="\x00\x00\x18\x42"
floats[39]="\x00\x00\x1c\x42"
floats[40]="\x00\x00\x20\x42"

floats[300]="\x00\x00\x96\x43"
floats[350]="\x00\x00\xaf\x43"
floats[400]="\x00\x00\xc8\x43"
floats[450]="\x00\x00\xe1\x43"
floats[500]="\x00\x00\xfa\x43"
floats[550]="\x00\x80\x09\x44"
floats[600]="\x00\x00\x16\x44"

uint8s=()
uint8s[0]="\x00"
uint8s[1]="\x01"
uint8s[2]="\x02"
uint8s[3]="\x03"
uint8s[4]="\x04"
uint8s[5]="\x05"
uint8s[6]="\x06"
uint8s[7]="\x07"
uint8s[8]="\x08"
uint8s[9]="\x09"
uint8s[10]="\x0a"

if [[ version -eq 1 ]]; then

hi_press_n=${#hi_press_vals[@]}
lo_press_n=${#lo_press_vals[@]}
breaths_n=${#breaths_vals[@]}
br_prop_n=${#br_prop_vals[@]}
air_vol_n=${#air_vol_vals[@]}
hi_press_idx=0
lo_press_idx=0
breaths_idx=0
br_prop_idx=0
air_vol_idx=0
iteration=0
while [[ iteration -lt iterations ]]; do
  hi_press_bytes=${floats[${hi_press_vals[$hi_press_idx]}]}
  lo_press_bytes=${floats[${lo_press_vals[$lo_press_idx]}]}
  breaths_bytes=${floats[${breaths_vals[$breaths_idx]}]}
  br_prop_bytes=${floats[${br_prop_vals[$br_prop_idx]}]}
  air_vol_bytes=${floats[${air_vol_vals[$air_vol_idx]}]}

  echo -n -e "\xf0\x0f\x01\x20${hi_press_bytes}${lo_press_bytes}${breaths_bytes}${br_prop_bytes}${air_vol_bytes}\x00\x00\x00\x00\x00\x00\x00\xaa" > ${target}

  sleep $interval

  iteration=$(( iteration + 1 ))
  hi_press_idx=$(( (hi_press_idx + 1) % hi_press_n ))
  lo_press_idx=$(( (lo_press_idx + 1) % lo_press_n ))
  breaths_idx=$(( (breaths_idx + 1) % breaths_n ))
  br_prop_idx=$(( (br_prop_idx + 1) % br_prop_n ))
  air_vol_idx=$(( (air_vol_idx + 1) % air_vol_n ))
done

elif [[ version -eq 2 ]]; then

press_n=${#press_vals[@]}
flow_n=${#flow_vals[@]}
ipm_n=${#ipm_vals[@]}
epm_n=${#epm_vals[@]}
inh_vol_n=${#inh_vol_vals[@]}
inh_vol_per_min_n=${#inh_vol_per_min_vals[@]}
exh_vol_n=${#exh_vol_vals[@]}
breaths_per_min_n=${#breaths_per_min_vals[@]}
lung_compl_n=${#lung_compl_vals[@]}
rot_target_n=${#rot_target_vals[@]}
lst_inh_typ_n=${#lst_inh_typ_vals[@]}
breath_phase_n=${#breath_phase_vals[@]}
press_idx=0
flow_idx=0
ipm_idx=0
epm_idx=0
inh_vol_idx=0
inh_vol_per_min_idx=0
exh_vol_idx=0
breaths_per_min_idx=0
lung_compl_idx=0
rot_target_idx=0
lst_inh_typ_idx=0
breath_phase_idx=0
iteration=0
while [[ iteration -lt iterations ]]; do
  press_bytes=${floats[${press_vals[$press_idx]}]}
  flow_bytes=${floats[${flow_vals[$flow_idx]}]}
  ipm_bytes=${floats[${ipm_vals[$ipm_idx]}]}
  epm_bytes=${floats[${epm_vals[$epm_idx]}]}
  inh_vol_bytes=${floats[${inh_vol_vals[$inh_vol_idx]}]}
  inh_vol_per_min_bytes=${floats[${inh_vol_per_min_vals[$inh_vol_per_min_idx]}]}
  exh_vol_bytes=${floats[${exh_vol_vals[$exh_vol_idx]}]}
  breaths_per_min_bytes=${floats[${breaths_per_min_vals[$breaths_per_min_idx]}]}
  lung_compl_bytes=${floats[${lung_compl_vals[$lung_compl_idx]}]}
  rot_target_bytes=${floats[${rot_target_vals[$rot_target_idx]}]}
  lst_inh_typ_byte=${uint8s[${lst_inh_typ_vals[$lst_inh_typ_idx]}]}
  breath_phase_byte=${uint8s[${breath_phase_vals[$breath_phase_idx]}]}

  echo -n -e "\xf0\x0f\x01\x31${press_bytes}${flow_bytes}${ipm_bytes}${epm_bytes}${inh_vol_bytes}${inh_vol_per_min_bytes}${exh_vol_bytes}${breaths_per_min_bytes}${lung_compl_bytes}${rot_target_bytes}${lst_inh_typ_byte}${breath_phase_byte}\x00\x00\xaa" > ${target}

  sleep $interval

  iteration=$(( iteration + 1 ))
  press_idx=$(( (press_idx + 1) % press_n ))
  flow_idx=$(( (flow_idx + 1) % flow_n ))
  ipm_idx=$(( (ipm_idx + 1) % ipm_n ))
  epm_idx=$(( (epm_idx + 1) % epm_n ))
  inh_vol_idx=$(( (inh_vol_idx + 1) % inh_vol_n ))
  inh_vol_per_min_idx=$(( (inh_vol_per_min_idx + 1) % inh_vol_per_min_n ))
  exh_vol_idx=$(( (exh_vol_idx + 1) % exh_vol_n ))
  breaths_per_min_idx=$(( (breaths_per_min_idx + 1) % breaths_per_min_n ))
  lung_compl_idx=$(( (lung_compl_idx + 1) % lung_compl_n ))
  rot_target_idx=$(( (rot_target_idx + 1) % rot_target_n ))
  lst_inh_typ_idx=$(( (lst_inh_typ_idx + 1) % lst_inh_typ_n ))
  breath_phase_idx=$(( (breath_phase_idx + 1) % breath_phase_n ))
done

fi

echo "Done."
