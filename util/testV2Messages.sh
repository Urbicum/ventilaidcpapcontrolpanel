#!/bin/bash

# Based on fakeDataSender, this script sends one of each new type of messages.
# Contents are just increasing with fields integers.
# For now they should all be logged in "generic" logfile.
#
# Testing should be done with --newApi set in command line arguments
#
# Before running this script, one must open a fake connection using socat:
#   socat -d -d pty,raw,echo=0 pty,raw,echo=0
# This will display two pseudo-terminals (for example: /dev/pts/1 and /dev/pts/2).
# The Control Panel application must listen to one of them
# and the second one should be passed as an argument to this script.
#
# Then you can run the script for example like this:
# ./testV2Messages.sh /dev/pts/1
#
# The Control Panel application should then be able to connect to /dev/pts/2 and receive our fake data.
# Remember to run it with the --skipCrc argument because our fake data have wrong CRC values.

if [[ -z "$1" ]]; then
  echo "You need to specify a pseudo-terminal to which the data will be sent. E.g.:"
  echo "  $0 /dev/pts/1"
  echo "Available pseudo-terminals:"
  ls /dev/pts/*
  exit 1
fi

target="$1"
F1="\x00\x00\x80\x3f"
F2="\x00\x00\x00\x40"
F3="\x00\x00\x40\x40"
F4="\x00\x00\x80\x40"
F5="\x00\x00\xa0\x40"
F6="\x00\x00\xc0\x40"
F7="\x00\x00\xe0\x40"
F8="\x00\x00\x00\x41"
F9="\x00\x00\x10\x41"
F10="\x00\x00\x20\x41"
F11="\x00\x00\x30\x41"
F12="\x00\x00\x40\x41"
F13="\x00\x00\x50\x41"
F14="\x00\x00\x60\x41"
F15="\x00\x00\x70\x41"

U1="\x01\x00\x00\x00"
U2="\x02\x00\x00\x00"

# MainParamsMessage
echo -n -e "\xf0\x0f\x01\x31$F1$F2$F3$F4$F5$F6$F7$F8$F9$F10\x0b\x0c\x00\x00\xaa" > ${target}

# MedicalParamsMessage
echo -n -e "\xf0\x0f\x02\x20$F1$F2$F3$F4$F5$F6\x07\x00\x00\xaa" > ${target}

# PIDParamsMessage
echo -n -e "\xf0\x0f\x03\x43$F1$F2$F3$F4$F5$F6$F7$F8$F9$F10$F11$F12$F13$F14$F15\x00\x00\xaa" > ${target}

# FlowCalibrationMessage
echo -n -e "\xf0\x0f\x04\x1b$F1$F2$F3$F4$F5\x00\x00\xaa" > ${target}

# WorkpointCalibrationMessage
echo -n -e "\xf0\x0f\x05\x1b$F1$F2$F3$F4$F5\x00\x00\xaa" > ${target}

# BreathAnalyserMessage
echo -n -e "\xf0\x0f\x06\x17$F1$F2$F3$F4\x00\x00\xaa" > ${target}

# AlarmStatusMessage
echo -n -e "\xf0\x0f\x0a\x0b$U1\x00\x00\xaa" > ${target}

# AlarmDetailsMessage
echo -n -e "\xf0\x0f\x0b\x0f$U1$U2\x00\x00\xaa" > ${target}

# PressureS0CalibMessage
echo -n -e "\xf0\x0f\x14\x17$F1$F2$F3$F4\x00\x00\xaa" > ${target}

# PressureS1CalibMessage
echo -n -e "\xf0\x0f\x15\x17$F1$F2$F3$F4\x00\x00\xaa" > ${target}

# PressureS2CalibMessage
echo -n -e "\xf0\x0f\x16\x17$F1$F2$F3$F4\x00\x00\xaa" > ${target}

# PressureS3CalibMessage
echo -n -e "\xf0\x0f\x17\x17$F1$F2$F3$F4\x00\x00\xaa" > ${target}

# DebugV2Message
echo -n -e "\xf0\x0f\x50\x37$F1$F2$F3$F4$F5$F6$F7$F8$F9$F10$F11$F12\x00\x00\xaa" > ${target}

echo "Done."
