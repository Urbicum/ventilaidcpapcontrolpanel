import RPi.GPIO as GPIO
import os

pinNUmber = 17
GPIO.setmode(GPIO.BCM)
GPIO.setup(pinNUmber, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

while True:
  print('Waiting for kill button...')
  GPIO.wait_for_edge(pinNUmber, GPIO.RISING)
  print('Killing VentilAid process')
  os.system('pkill VentilAid')

