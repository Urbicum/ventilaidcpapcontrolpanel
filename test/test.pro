QT = core testlib serialport

TARGET = test_ventilaid
CONFIG += qt warn_on depend_includepath testcase

TEMPLATE = app

INCDIR = ../src
INCLUDEPATH += $$INCDIR

HEADERS += \
    $$INCDIR/serial.h \
    $$INCDIR/message/alertMessage.h \
    $$INCDIR/message/alarmStatusMessage.h \
    $$INCDIR/message/alarmDetailsMessage.h \
    $$INCDIR/message/alarmThresholdMessage.h \
    $$INCDIR/message/annotatedMessage.h \
    $$INCDIR/message/baseMessage.h \
    $$INCDIR/message/debugMessage.h \
    $$INCDIR/message/debugV2Message.h \
    $$INCDIR/message/flowCalibrationMessage.h \
    $$INCDIR/message/errorMessage.h \
    $$INCDIR/message/mainParamsMessage.h \
    $$INCDIR/message/medicalParamsMessage.h \
    $$INCDIR/message/pidParamsMessage.h \
    $$INCDIR/message/pressureSensorCalibMessage.h \
    $$INCDIR/message/requestMessage.h \
    $$INCDIR/message/flowCalibrationMessage.h \
    $$INCDIR/message/workpointCalibrationMessage.h \
    $$INCDIR/message/breathAnalyserMessage.h \
    $$INCDIR/message/mainParamsMessage.h \
    $$INCDIR/message/statusMessage.h

SOURCES += \
    $$INCDIR/serial.cpp \
    $$INCDIR/message/alertMessage.cpp \
    $$INCDIR/message/alarmStatusMessage.cpp \
    $$INCDIR/message/alarmDetailsMessage.cpp \
    $$INCDIR/message/alarmThresholdMessage.cpp \
    $$INCDIR/message/baseMessage.cpp \
    $$INCDIR/message/debugMessage.cpp \
    $$INCDIR/message/debugV2Message.cpp \
    $$INCDIR/message/errorMessage.cpp \
    $$INCDIR/message/mainParamsMessage.cpp \
    $$INCDIR/message/medicalParamsMessage.cpp \
    $$INCDIR/message/pidParamsMessage.cpp \
    $$INCDIR/message/pressureSensorCalibMessage.cpp \
    $$INCDIR/message/requestMessage.cpp \
    $$INCDIR/message/flowCalibrationMessage.cpp \
    $$INCDIR/message/workpointCalibrationMessage.cpp \
    $$INCDIR/message/breathAnalyserMessage.cpp \
    $$INCDIR/message/statusMessage.cpp


SOURCES += test_serial.cpp

