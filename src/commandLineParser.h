#ifndef COMMANDLINEPARSER_H
#define COMMANDLINEPARSER_H

#include "runtimeConfig.h"

#include <QCommandLineParser>

class CommandLineParser : public QCommandLineParser {
public:
    CommandLineParser();
    RuntimeConfig validateAndGetRuntimeConfig();

private:
    QCommandLineOption baudRate;
    QCommandLineOption serialPort;
    QCommandLineOption skipCrc;
    QCommandLineOption runHeadless;
};

#endif // COMMANDLINEPARSER_H
