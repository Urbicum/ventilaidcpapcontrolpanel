#include "serialOverseer.h"
#include "runtimeConfig.h"

SerialOverseer::SerialOverseer(QObject* parent, Utils::Target target, RuntimeConfig* runtimeConfig)
    : QObject(parent)
    , serial(new Serial(this, runtimeConfig->skipCrc()))
    , sendTimer(new QTimer(this))
    , reconnectTimer(new QTimer(this))
    , state(State::Normal)
    , messageHandler(nullptr)
{
    connect(serial, &Serial::connected, [this, target](QString portName) {
        qDebug() << "Connected to:" << portName;

        emit connected(portName);

        if (isReconnecting())
            stopReconnecting();

        QSettings settings;
        if (settings.value("gatherLogs").toBool() && (target != Utils::Target::Raspberry)) {
            messageHandler = new MessageHandler(this);
            connect(serial, &Serial::newStatusMessage, messageHandler, &MessageHandler::newStatusMessage);
            connect(serial, &Serial::newDebugMessage, messageHandler, &MessageHandler::newDebugMessage);
            connect(serial, &Serial::newAlertMessage, messageHandler, &MessageHandler::newAlertMessage);
            connect(serial, &Serial::newErrorMessage, messageHandler, &MessageHandler::newErrorMessage);
            connect(serial, &Serial::newGenericMessage, messageHandler, &MessageHandler::newGenericMessage);
        }

        sendTimer->start(settings.value("sendIntervalMs").toInt());
    });

    connect(serial, &Serial::disconnected, [this](Serial::DisconnectionReason reason, QString msg) {
        emit disconnected(reason, msg);

        if (messageHandler) {
            delete messageHandler;
            messageHandler = nullptr;
        }

        sendTimer->stop();

        // Report errors
        if (reason == Serial::DisconnectionReason::Error)
            qDebug() << "Serial port error: " << msg;

        if (QSettings().value("autoReconnect").toBool() && reason == Serial::DisconnectionReason::Error && state == State::Normal) {
            state = State::Reconnecting;
            reconnectTimer->start(1000);
            emit reconnectingStarted(msg);
        }
    });

    connect(reconnectTimer, &QTimer::timeout, this, &SerialOverseer::reopenLastSerial);
    connect(sendTimer, &QTimer::timeout, this, &SerialOverseer::statusMessageRequest);
    connect(serial, &Serial::newStatusMessage, this, &SerialOverseer::newStatusMessage);
    connect(serial, &Serial::newDebugMessage, this, &SerialOverseer::newDebugMessage);
    connect(serial, &Serial::newGenericMessage, this, &SerialOverseer::newGenericMessage);
    connect(this, &SerialOverseer::sendNewGenericMessage, serial, &Serial::onSendNewGenericMessage);
    connect(serial, &Serial::newChangeParamsMessage, this, &SerialOverseer::newChangeParamsMessage);
}

SerialOverseer::~SerialOverseer()
{
    qDebug() << "~SerialOverseer";
}

bool SerialOverseer::isConnected()
{
    return serial->isConnected();
}

void SerialOverseer::disconnect()
{
    serial->disconnect();
}

bool SerialOverseer::isReconnecting()
{
    return state == State::Reconnecting;
}

void SerialOverseer::stopReconnecting()
{
    reconnectTimer->stop();
    state = State::Normal;
}

void SerialOverseer::sendMessage(StatusMessage msg)
{
    serial->sendMessage(msg);
}

void SerialOverseer::sendMessage(RequestMessage msg)
{
    serial->sendMessage(msg);
}

void SerialOverseer::sendChangeParamsMessage(QSharedPointer<const AnnotatedMessage> msg)
{
    serial->sendChangeParamsMessage(msg);
}

void SerialOverseer::open(QString portName, Serial::BaudRate baudRate)
{
    lastSerial = portName;
    lastBaudRate = baudRate;

    serial->open(portName, baudRate);
}

void SerialOverseer::reopenLastSerial()
{
    serial->open(lastSerial, lastBaudRate);
}
