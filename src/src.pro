QT       += core gui serialport charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VentilAid
TEMPLATE = app

include(GitVersion.pri)

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
CONFIG += c++17

SOURCES += \
        commandLineParser.cpp \
        aboutDialog.cpp \
        chartWidget.cpp \
        debugDialog.cpp \
        editMessageDialog.cpp \
        fileLogger.cpp \
        main.cpp \
        mainWindow.cpp \
        message/alarmDetailsMessage.cpp \
        message/alarmStatusMessage.cpp \
        message/alarmThresholdMessage.cpp \
        message/alertMessage.cpp \
        message/baseMessage.cpp \
        message/breathAnalyserMessage.cpp \
        message/debugMessage.cpp \
        message/debugV2Message.cpp \
        message/errorMessage.cpp \
        message/flowCalibrationMessage.cpp \
        message/mainParamsMessage.cpp \
        message/medicalParamsMessage.cpp \
        message/pidParamsMessage.cpp \
        message/pressureSensorCalibMessage.cpp \
        message/requestAlarmThresholdMessage.cpp \
        message/requestMessage.cpp \
        message/workpointCalibrationMessage.cpp \
        messageHandler.cpp \
        portSelectionDialog.cpp \
        runtimeConfig.cpp \
        secondaryScreenDialog.cpp \
        serialOverseer.cpp \
        sendMessageDialog.cpp \
        serial.cpp \
        message/statusMessage.cpp \
        setDialog.cpp \
        settingsDialog.cpp \
        settingsScreenDialog.cpp \
        utils.cpp \
        widget/sliderWidget.cpp \
        widget/valueWidget.cpp

HEADERS += \
        commandLineParser.h \
        aboutDialog.h \
        chartWidget.h \
        debugDialog.h \
        editMessageDialog.h \
        fileLogger.h \
        mainWindow.h \
        message/alarmDetailsMessage.h \
        message/alarmStatusMessage.h \
        message/alarmThresholdMessage.h \
        message/alertMessage.h \
        message/annotatedMessage.h \
        message/baseMessage.h \
        message/breathAnalyserMessage.h \
        message/debugMessage.h \
        message/debugV2Message.h \
        message/errorMessage.h \
        message/flowCalibrationMessage.h \
        message/mainParamsMessage.h \
        message/medicalParamsMessage.h \
        message/pidParamsMessage.h \
        message/pressureSensorCalibMessage.h \
        message/requestAlarmThresholdMessage.h \
        message/requestMessage.h \
        message/statusMessage.h \
        message/workpointCalibrationMessage.h \
        messageHandler.h \
        portSelectionDialog.h \
        runtimeConfig.h \
        secondaryScreenDialog.h \
        serialOverseer.h \
        sendMessageDialog.h \
        serial.h \
        setDialog.h \
        settingsDialog.h \
        settingsScreenDialog.h \
        utils.h \
        widget/sliderWidget.h \
        widget/valueWidget.h

FORMS += \
        aboutDialog.ui \
        chartWidget.ui \
        debugDialog.ui \
        editMessageDialog.ui \
        mainWindow.ui \
        portSelectionDialog.ui \
        secondaryScreenDialog.ui \
        sendMessageDialog.ui \
        setDialog.ui \
        settingsDialog.ui \
        settingsScreenDialog.ui \
        widget/sliderWidget.ui \
        widget/valueWidget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    ../resource.qrc

unix {
    isEmpty(PREFIX) {
        PREFIX = /usr/local
    }

    target.path = $$PREFIX/bin

    shortcutfiles.files = ../util/VentilAid.desktop
    shortcutfiles.path = $$PREFIX/share/applications/

    data.files += ../resource/icon_256x256.png
    data.path = $$PREFIX/

    INSTALLS += shortcutfiles
    INSTALLS += data
}

DISTFILES += \
    ../util/VentilAid.desktop

OTHER_FILES += \
    ../.gitignore \
    ../.clang-format \
    ../.gitlab-ci.yml \
    ../util/emul.cpp \
    ../README.md
