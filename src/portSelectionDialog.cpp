#include "portSelectionDialog.h"
#include "ui_portSelectionDialog.h"

#include "serial.h"

#include <QSettings>

PortSelectionDialog::PortSelectionDialog(QWidget* parent)
    : QDialog(parent)
    , ui(new Ui::PortSelectionDialog)
{
    ui->setupUi(this);

    const auto availablePorts = QSerialPortInfo::availablePorts();

    for (const auto& port : availablePorts)
        ui->portComboBox->addItem(port.portName());

    int idx = ui->portComboBox->findText(QSettings().value("lastSelectedPort").toString());
    if (idx != -1)
        ui->portComboBox->setCurrentIndex(idx);

    ui->baudRatecomboBox->addItem(QString::number(Serial::BaudRate::Baud460800), Serial::BaudRate::Baud460800);
    ui->baudRatecomboBox->addItem(QString::number(Serial::BaudRate::Baud230400), Serial::BaudRate::Baud230400);
    ui->baudRatecomboBox->addItem(QString::number(Serial::BaudRate::Baud115200), Serial::BaudRate::Baud115200);
    ui->baudRatecomboBox->addItem(QString::number(Serial::BaudRate::Baud57600), Serial::BaudRate::Baud57600);
    ui->baudRatecomboBox->addItem(QString::number(Serial::BaudRate::Baud38400), Serial::BaudRate::Baud38400);
    ui->baudRatecomboBox->addItem(QString::number(Serial::BaudRate::Baud19200), Serial::BaudRate::Baud19200);
    ui->baudRatecomboBox->addItem(QString::number(Serial::BaudRate::Baud9600), Serial::BaudRate::Baud9600);
    ui->baudRatecomboBox->addItem(QString::number(Serial::BaudRate::Baud4800), Serial::BaudRate::Baud4800);
    ui->baudRatecomboBox->addItem(QString::number(Serial::BaudRate::Baud2400), Serial::BaudRate::Baud2400);
    ui->baudRatecomboBox->addItem(QString::number(Serial::BaudRate::Baud1200), Serial::BaudRate::Baud1200);

    idx = ui->baudRatecomboBox->findText(QString::number(QSettings().value("lastSelectedBaudRate").toInt()));
    if (idx != -1)
        ui->baudRatecomboBox->setCurrentIndex(idx);

    connect(this, &QDialog::accepted, [this]() {
        QSettings settings;
        settings.setValue("lastSelectedPort", ui->portComboBox->currentText());
        settings.setValue("lastSelectedBaudRate", ui->baudRatecomboBox->currentData().toInt());
    });
}

PortSelectionDialog::~PortSelectionDialog()
{
    delete ui;
}

Serial::BaudRate PortSelectionDialog::getBaudRate() const
{
    uint baudRate = ui->baudRatecomboBox->currentData().toUInt();
    return static_cast<Serial::BaudRate>(baudRate);
}

QString PortSelectionDialog::getSeriaPortName() const
{
    return ui->portComboBox->currentText();
}
