#include "runtimeConfig.h"

RuntimeConfig::RuntimeConfig(bool skipCrc, bool runHeadless)
    : _skipCrc(skipCrc)
    , _runHeadless(runHeadless)
{
}

bool RuntimeConfig::skipCrc() const
{
    return _skipCrc;
}

QString RuntimeConfig::getSerialPort() const
{
    return serialPort;
}

uint32_t RuntimeConfig::getBaudRate() const
{
    return baudRate;
}

bool RuntimeConfig::runHeadless() const
{
    return _runHeadless;
}

bool RuntimeConfig::isSerialPortSet() const
{
    return !serialPort.isEmpty();
}
