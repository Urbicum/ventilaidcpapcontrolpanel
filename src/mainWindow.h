#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "utils.h"

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class QProgressDialog;
class EditMessageDialog;
class DebugDialog;
class SerialOverseer;
class SecondaryScreenDialog;
class SettingsScreenDialog;
class BaseMessage;
struct MainParamsMessage;

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    static constexpr float SLIDER_FACTOR = 10.f;

    explicit MainWindow(QWidget* parent, Utils::Target target, SerialOverseer* serial);
    ~MainWindow();

private slots:
    void onConnectionStausChanged(bool connected, QString portName);
    void onNewGenericMessage(QSharedPointer<const BaseMessage> msg);
    void openUserSelectedSerial();

private:
    void setupMenu();
    void setupCharts();
    void setupValueWidgets();

    Ui::MainWindow* ui;

    const Utils::Target target;
    QProgressDialog* progressDialog;
    DebugDialog* debugDialog;
    EditMessageDialog* editDialog;
    SecondaryScreenDialog* secondaryScreenDialog;
    SerialOverseer* serialOverseer;
    SettingsScreenDialog* settingsScreenDialog;
};

#endif // MAINWINDOW_H
