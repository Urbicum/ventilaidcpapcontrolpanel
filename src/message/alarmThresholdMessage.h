#ifndef ALARMTHRESHOLDMESSAGE_H
#define ALARMTHRESHOLDMESSAGE_H

#include "message/baseMessage.h"

struct AlarmThresholdMessage : public BaseMessage {
public:
    const static uint8_t SIZE;
    const static uint8_t MSG_ID;
    const static uint8_t SEND_MSG_ID;

    AlarmThresholdMessage(const QByteArray& data);
    AlarmThresholdMessage(float alarmLow, float warnLow, float warnHigh, float alarmHigh, uint8_t alarm, uint8_t detail);

    QString format() const final;
    QByteArray getData() const final;

    float alarmLowValue;
    float warnLowValue;
    float warnHighValue;
    float alarmHighValue;
    uint8_t alarmNo;
    uint8_t detailNo;
};

#endif // ALARMTHRESHOLDMESSAGE_H
