#include "message/mainParamsMessage.h"

const uint8_t MainParamsMessage::MSG_ID = 1;
const uint8_t MainParamsMessage::SIZE = 49;

MainParamsMessage::MainParamsMessage(const QByteArray& data)
{
    const char* ptr = data.data();

    pressure = *((float*)(ptr + 4));
    flow = *((float*)(ptr + 8));
    inhalePressureMean = *((float*)(ptr + 12));
    exhalePressureMean = *((float*)(ptr + 16));
    inhaleVol = *((float*)(ptr + 20));
    inhaleVolPerMin = *((float*)(ptr + 24));
    exhaleVol = *((float*)(ptr + 28));
    breathPerMin = *((float*)(ptr + 32));
    lungCompl = *((float*)(ptr + 36));
    rotTarget = *((float*)(ptr + 40));
    lastInhaleType = *((uint8_t*)(ptr + 44));
    breathPhase = *((uint8_t*)(ptr + 45));
}

QString MainParamsMessage::format() const
{
    return QString(";%1;%2;%3;%4;%5;%6;%7;%8;%9;%10;%11;%12;%13\n")
        .arg(MainParamsMessage::MSG_ID)
        .arg(pressure)
        .arg(flow)
        .arg(inhalePressureMean)
        .arg(exhalePressureMean)
        .arg(inhaleVol)
        .arg(inhaleVolPerMin)
        .arg(exhaleVol)
        .arg(breathPerMin)
        .arg(lungCompl)
        .arg(rotTarget)
        .arg(static_cast<uint16_t>(lastInhaleType))
        .arg(static_cast<uint16_t>(breathPhase));
}

QString MainParamsMessage::formatInhaleType() const
{
    switch (lastInhaleType) {
    case 1:
        return "spontaneous";
    case 2:
        return "forced";
    default: // 0
        return "error";
    }
}

QString MainParamsMessage::formatBreathPhase() const
{
    switch (breathPhase) {
    case 1:
        return "inhale";
    case 2:
        return "exhale";
    case 3:
        return "leak";
    case 4:
        return "end of inhale";
    case 5:
        return "end of exhale";
    case 6:
        return "cough";
    default: // 0
        return "error";
    }
}
