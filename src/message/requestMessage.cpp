#include "requestMessage.h"

const uint8_t RequestMessage::MSG_ID = 100;
const uint8_t RequestMessage::SIZE = 8;

QByteArray RequestMessage::getData() const
{
    QByteArray data(SIZE, 0);
    char* ptr = data.data();

    // header
    *((uint8_t*)(ptr)) = START_MARKER_ARRAY[0];
    *((uint8_t*)(ptr + 1)) = START_MARKER_ARRAY[1];
    *((uint8_t*)(ptr + 2)) = MSG_ID;
    *((uint8_t*)(ptr + 3)) = SIZE;

    // data
    *((uint8_t*)(ptr + 4)) = messageId;

    // footer
    const uint8_t payloadSize = SIZE - BaseMessage::HEADER_SIZE - BaseMessage::FOOTER_SIZE;
    *((uint16_t*)(ptr + 5)) = qChecksum((char*)(ptr + BaseMessage::HEADER_SIZE), payloadSize, Qt::ChecksumType::ChecksumIso3309);

    *((uint8_t*)(ptr + 7)) = END_BYTE;

    return data;
}
