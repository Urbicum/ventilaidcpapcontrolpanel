#ifndef ALARMDETAILSMESSAGE_H
#define ALARMDETAILSMESSAGE_H

#include "message/baseMessage.h"

struct AlarmDetailsMessage : public BaseMessage {
    const static uint8_t SIZE;
    const static uint8_t MSG_ID;

    AlarmDetailsMessage(const QByteArray& data);

    QString format() const final;

    uint32_t code;
    uint32_t detail;
};

#endif // ALARMDETAILSMESSAGE_H
