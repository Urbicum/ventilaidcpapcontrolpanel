#ifndef BASEMESSAGE_H
#define BASEMESSAGE_H

#include <QByteArray>
#include <QString>

#include <cstdint>

class BaseMessage {
public:
    static const QByteArray START_MARKER_ARRAY;
    static const int HEADER_SIZE = 4;
    static const int FOOTER_SIZE = 3;
    static const uint8_t END_BYTE = 0xAA;

    BaseMessage() = default;

    virtual ~BaseMessage() = default;

    virtual QByteArray getData() const { return QByteArray(); }
    virtual QString format() const { return QString(); }
};

#endif // BASEMESSAGE_H
