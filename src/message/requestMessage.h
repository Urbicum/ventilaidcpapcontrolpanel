#ifndef REQUESTMESSAGE_H
#define REQUESTMESSAGE_H

#include "message/baseMessage.h"

class RequestMessage : public BaseMessage {
public:
    const static uint8_t SIZE;
    const static uint8_t MSG_ID;

    RequestMessage(uint8_t messageId)
        : messageId(messageId)
    {
    }

    QByteArray getData() const;

private:
    uint8_t messageId;
};

#endif // REQUESTMESSAGE_H
