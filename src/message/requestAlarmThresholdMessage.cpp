#include "requestAlarmThresholdMessage.h"

const uint8_t RequestAlarmThresholdMessage::MSG_ID = 101;
const uint8_t RequestAlarmThresholdMessage::SIZE = 9;

QByteArray RequestAlarmThresholdMessage::getData() const
{
    QByteArray data(SIZE, 0);
    char* ptr = data.data();

    // header
    *((uint8_t*)(ptr)) = START_MARKER_ARRAY[0];
    *((uint8_t*)(ptr + 1)) = START_MARKER_ARRAY[1];
    *((uint8_t*)(ptr + 2)) = MSG_ID;
    *((uint8_t*)(ptr + 3)) = SIZE;

    // data
    *((uint8_t*)(ptr + 4)) = alarmNo;
    *((uint8_t*)(ptr + 5)) = detailNo;

    // footer
    const uint8_t payloadSize = SIZE - BaseMessage::HEADER_SIZE - BaseMessage::FOOTER_SIZE;
    *((uint16_t*)(ptr + 6)) = qChecksum((char*)(ptr + BaseMessage::HEADER_SIZE), payloadSize, Qt::ChecksumType::ChecksumIso3309);

    *((uint8_t*)(ptr + 8)) = END_BYTE;

    return data;
}
