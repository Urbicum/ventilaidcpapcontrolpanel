#include "message/pressureSensorCalibMessage.h"

const uint8_t PressureSensorCalibMessage::SENSOR_NUM = 4;
const uint8_t PressureSensorCalibMessage::MSG_IDS[SENSOR_NUM] = {
    20, 21, 22, 23
};
const uint8_t PressureSensorCalibMessage::SEND_MSG_IDS[SENSOR_NUM] = {
    120, 121, 122, 123
};
const uint8_t PressureSensorCalibMessage::SIZE = 23;
const uint8_t PressureSensorCalibMessage::MEMBER_NUM = 4;
const char* PressureSensorCalibMessage::FIELD_NAMES[PressureSensorCalibMessage::MEMBER_NUM] = {
    "C0",
    "C1",
    "C2",
    "C3"
};
const MemberInfo PressureSensorCalibMessage::MEMBER_INFO[PressureSensorCalibMessage::MEMBER_NUM]{
    { 0, DataType::FLOAT },
    { 4, DataType::FLOAT },
    { 8, DataType::FLOAT },
    { 12, DataType::FLOAT }
};

QString PressureSensorCalibMessage::format() const
{
    return QString(";%1;%2;%3;%4;%5\n")
        .arg(getId())
        .arg(C0())
        .arg(C1())
        .arg(C2())
        .arg(C3());
}
