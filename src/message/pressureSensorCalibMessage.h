#ifndef PRESSURESENSORCALIBMESSAGE_h
#define PRESSURESENSORCALIBMESSAGE_h

#include "message/annotatedMessage.h"

struct PressureSensorCalibMessage : public AnnotatedMessage {
    const static uint8_t SIZE;
    const static uint8_t SENSOR_NUM;
    const static uint8_t MSG_IDS[];
    const static uint8_t SEND_MSG_IDS[];
    const static uint8_t MEMBER_NUM;
    static const char* FIELD_NAMES[];
    static const MemberInfo MEMBER_INFO[];

    PressureSensorCalibMessage(QByteArray data, uint8_t idx)
        : AnnotatedMessage(data)
    {
        assert(idx >= 0 && idx <= 3);
        sensorIdx = idx;
    }

    QString format() const final;

    QSharedPointer<AnnotatedMessage> clone() const override
    {
        return QSharedPointer<PressureSensorCalibMessage>::create(getPayload(), sensorIdx);
    }

    uint8_t getSize() const final { return PressureSensorCalibMessage::SIZE; }
    uint8_t getId() const final { return PressureSensorCalibMessage::MSG_IDS[sensorIdx]; }
    uint8_t getSendId() const final { return PressureSensorCalibMessage::SEND_MSG_IDS[sensorIdx]; }
    uint8_t getMemberNum() const final { return PressureSensorCalibMessage::MEMBER_NUM; }
    const char** getFieldNames() const final { return PressureSensorCalibMessage::FIELD_NAMES; }
    const MemberInfo* getMemberInfo() const final { return PressureSensorCalibMessage::MEMBER_INFO; }

    float C0() const { return *(float*)(data.data() + 0); }
    float C1() const { return *(float*)(data.data() + 4); }
    float C2() const { return *(float*)(data.data() + 8); }
    float C3() const { return *(float*)(data.data() + 12); }

private:
    uint8_t sensorIdx;
};

#endif // PRESSURESENSORCALIBMESSAGE_H
