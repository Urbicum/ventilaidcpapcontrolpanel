#ifndef ANNOTATEDMESSAGE_H
#define ANNOTATEDMESSAGE_H

#include "message/baseMessage.h"

#include <QSharedPointer>
#include <QVariant>

class AnnotatedMessage : public BaseMessage {
public:
    enum DataType : uint8_t {
        FLOAT = 1,
        UINT8_T = 2
    };

    struct MemberInfo {
        uint8_t alignment;
        DataType memberType;
    };

    class DataIterator {
    public:
        DataIterator(const AnnotatedMessage* const msg, uint8_t idx = 0)
            : msg(msg)
            , idx(idx)
        {
        }

        struct return_type {
            const char* fieldName;
            DataType dataType;
            QVariant variantValue;
        };

        DataIterator& operator++()
        {
            idx++;
            return *this;
        }
        DataIterator operator++(int)
        {
            DataIterator it = *this;
            ++(*this);
            return it;
        }
        bool operator==(const DataIterator& other) const { return this->msg == other.msg && idx == other.idx; }
        bool operator!=(const DataIterator& other) const { return !(*this == other); }
        return_type operator*() const
        {
            return { msg->getFieldNames()[idx],
                msg->getMemberInfo()[idx].memberType,
                getValue(msg->getMemberInfo()[idx].memberType, msg->getPayload().data() + msg->getMemberInfo()[idx].alignment) };
        }

    private:
        QVariant getValue(DataType type, QByteArray::iterator it) const
        {
            switch (type) {
            case DataType::FLOAT:
                return QVariant(static_cast<float>(*(float*)(it)));
            case DataType::UINT8_T:
                return QVariant(static_cast<uint8_t>(*(uint8_t*)(it)));
            }
            return QVariant();
        }

        const AnnotatedMessage* const msg;
        size_t idx = 0;
    };

public:
    AnnotatedMessage(QByteArray payload)
        : data(payload)
    {
    }
    virtual ~AnnotatedMessage() = default;

    virtual QSharedPointer<AnnotatedMessage> clone() const = 0;

    QByteArray getPayload() const { return data; };
    void replacePayload(QByteArray&& array)
    {
        assert(array.size() == (getSize() - 7));
        data = array;
    }

    virtual uint8_t getSize() const = 0;
    virtual uint8_t getId() const = 0;
    virtual uint8_t getSendId() const = 0;
    virtual uint8_t getMemberNum() const = 0;
    virtual const char** getFieldNames() const = 0;
    virtual const MemberInfo* getMemberInfo() const = 0;

    const DataIterator begin() const { return DataIterator(this); }
    const DataIterator end() const { return DataIterator(this, getMemberNum()); }

protected:
    QByteArray data;
};

using DataIterator = AnnotatedMessage::DataIterator;
using DataType = AnnotatedMessage::DataType;
using MemberInfo = AnnotatedMessage::MemberInfo;

#endif // ANNOTATEDMESSAGE_H
