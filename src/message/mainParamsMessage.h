#ifndef MAINPARAMSMESSAGE_H
#define MAINPARAMSMESSAGE_H

#include "message/baseMessage.h"

struct MainParamsMessage : public BaseMessage {
public:
    const static uint8_t SIZE;
    const static uint8_t MSG_ID;

    MainParamsMessage(const QByteArray& data);

    QString format() const final;

    QString formatInhaleType() const;
    QString formatBreathPhase() const;

    float pressure;
    float flow;
    float inhalePressureMean;
    float exhalePressureMean;
    float inhaleVol;
    float inhaleVolPerMin;
    float exhaleVol;
    float breathPerMin;
    float lungCompl;
    float rotTarget;
    uint8_t lastInhaleType;
    uint8_t breathPhase;
};

#endif // MAINPARAMSMESSAGE_H
