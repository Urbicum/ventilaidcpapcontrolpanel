#ifndef DEBUGV2MESSAGE_H
#define DEBUGV2MESSAGE_H

#include "message/baseMessage.h"

struct DebugV2Message : public BaseMessage {
    const static uint8_t SIZE;
    const static uint8_t MSG_ID;

    DebugV2Message(const QByteArray& data);

    QString format() const final;

    float pressure0;
    float pressure1;
    float pressure2;
    float pressure3;
    float motorOut;
    float motorRPM;
    float motorCurr;
    float motorTemp;
    float deviceTemp;
    float ambientTemp;
    float ambientHumidity;
    float peepOut;
};

#endif // DEBUGV2MESSAGE_H
