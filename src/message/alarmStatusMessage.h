#ifndef ALARMSTATUSMESSAGE_H
#define ALARMSTATUSMESSAGE_H

#include "message/baseMessage.h"

struct AlarmStatusMessage : public BaseMessage {
public:
    const static uint8_t SIZE;
    const static uint8_t MSG_ID;

    AlarmStatusMessage(const QByteArray& data);

    QString format() const final;

    uint32_t code;
};

#endif // ALARMSTATUSMESSAGE_H
