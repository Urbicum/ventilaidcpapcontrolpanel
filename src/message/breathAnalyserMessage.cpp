#include "message/breathAnalyserMessage.h"

const uint8_t BreathAnalyserMessage::MSG_ID = 6;
const uint8_t BreathAnalyserMessage::SIZE = 23;

BreathAnalyserMessage::BreathAnalyserMessage(const QByteArray& data)
{
    const char* ptr = data.data();

    PTrig = *((float*)(ptr + 4));
    VTrig = *((float*)(ptr + 8));
    ESens = *((float*)(ptr + 12));
    ISens = *((float*)(ptr + 16));
}

QString BreathAnalyserMessage::format() const
{
    return QString(";%1;%2;%3;%4;%5\n")
        .arg(BreathAnalyserMessage::MSG_ID)
        .arg(PTrig)
        .arg(VTrig)
        .arg(ESens)
        .arg(ISens);
}
