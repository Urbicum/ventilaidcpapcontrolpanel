#include "statusMessage.h"

#include <QSettings>

const uint8_t StatusMessage::RECEIVE_MSG_ID = 1;
const uint8_t StatusMessage::SIZE = 32;

StatusMessage::StatusMessage(float highPressure, float lowPressure, float airVolume, float breathsPerMinute, float breathProportion)
    : highPressure(highPressure)
    , lowPressure(lowPressure)
    , airVolume(airVolume)
    , breathsPerMinute(breathsPerMinute)
    , breathProportion(breathProportion)
{
}

StatusMessage::StatusMessage(const QByteArray& data)
{
    // TODO: this assumes little endian

    const char* ptr = data.data();

    highPressure = *((float*)(ptr + 4));
    lowPressure = *((float*)(ptr + 8));
    breathsPerMinute = *((float*)(ptr + 12));
    breathProportion = *((float*)(ptr + 16));
    airVolume = *((float*)(ptr + 20));
}

QByteArray StatusMessage::getData() const
{
    // TODO: this assumes little endian

    QByteArray data(SIZE, 0);
    char* ptr = data.data();

    // header
    *((uint8_t*)(ptr)) = START_MARKER_ARRAY[0];
    *((uint8_t*)(ptr + 1)) = START_MARKER_ARRAY[1];
    *((uint8_t*)(ptr + 2)) = SEND_MSG_ID;
    *((uint8_t*)(ptr + 3)) = SIZE;

    // data
    *((float*)(ptr + 4)) = highPressure;
    *((float*)(ptr + 8)) = lowPressure;
    *((float*)(ptr + 12)) = breathsPerMinute;
    *((float*)(ptr + 16)) = breathProportion;
    *((float*)(ptr + 20)) = airVolume;

    // reserved for future use
    ptr[24] = ptr[25] = ptr[26] = ptr[27] = ptr[28] = 0;

    // footer
    const uint8_t payloadSize = SIZE - BaseMessage::HEADER_SIZE - BaseMessage::FOOTER_SIZE;
    *((uint16_t*)(ptr + 29)) = qChecksum((char*)(ptr + BaseMessage::HEADER_SIZE), payloadSize, Qt::ChecksumType::ChecksumIso3309);

    *((uint8_t*)(ptr + 31)) = END_BYTE;

    return data;
}

float StatusMessage::getHighPressure() const
{
    return highPressure;
}

float StatusMessage::getLowPressure() const
{
    return lowPressure;
}

float StatusMessage::getAirVolume() const
{
    return airVolume;
}

float StatusMessage::getBreathsPerMinute() const
{
    return breathsPerMinute;
}

float StatusMessage::getBreathProportion() const
{
    return breathProportion;
}
