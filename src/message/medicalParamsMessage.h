#ifndef MEDICALPARAMSMESSAGE_H
#define MEDICALPARAMSMESSAGE_H

#include "message/baseMessage.h"

struct MedicalParamsMessage : public BaseMessage {
public:
    const static uint8_t SIZE;
    const static uint8_t MSG_ID;
    const static uint8_t SEND_MSG_ID;

    MedicalParamsMessage(const QByteArray& data);
    MedicalParamsMessage(float PEEP, float targetFiFO2, float PIP, float IER, float breathPerMin, float targetVolume, uint8_t mod);

    QString format() const final;

    float PEEP;
    float targetFiFO2;
    float PIP;
    float IER;
    float breathPerMin;
    float targetVolume;
    uint8_t mode;

    QByteArray getData() const;
    QString formatMode() const;
};

#endif // MEDICALPARAMSMESSAGE_H
