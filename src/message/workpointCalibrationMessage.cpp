#include "message/workpointCalibrationMessage.h"

const uint8_t WorkpointCalibrationMessage::MSG_ID = 5;
const uint8_t WorkpointCalibrationMessage::SEND_MSG_ID = 105;
const uint8_t WorkpointCalibrationMessage::SIZE = 27;
const uint8_t WorkpointCalibrationMessage::MEMBER_NUM = 5;
const char* WorkpointCalibrationMessage::FIELD_NAMES[MEMBER_NUM] = {
    "C0",
    "C1",
    "C2",
    "C3",
    "C4"
};
const MemberInfo WorkpointCalibrationMessage::MEMBER_INFO[WorkpointCalibrationMessage::MEMBER_NUM]{
    { 0, DataType::FLOAT },
    { 4, DataType::FLOAT },
    { 8, DataType::FLOAT },
    { 12, DataType::FLOAT },
    { 16, DataType::FLOAT }
};

QString WorkpointCalibrationMessage::format() const
{
    return QString(";%1;%2;%3;%4;%5;%6\n")
        .arg(WorkpointCalibrationMessage::MSG_ID)
        .arg(C0())
        .arg(C1())
        .arg(C2())
        .arg(C3())
        .arg(C4());
}
