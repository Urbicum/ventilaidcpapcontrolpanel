#include "message/pidParamsMessage.h"

const uint8_t PIDParamsMessage::SIZE = 67;
const uint8_t PIDParamsMessage::MSG_ID = 3;
const uint8_t PIDParamsMessage::SEND_MSG_ID = 103;
const uint8_t PIDParamsMessage::MEMBER_NUM = 15;
const char* PIDParamsMessage::FIELD_NAMES[PIDParamsMessage::MEMBER_NUM] = {
    "MOTPID_P",
    "MOTPID_I",
    "MOTPID_D",
    "MOTPID_MAX",
    "MOTPID_MIN",
    "PRESPID_P",
    "PRESPID_I",
    "PRESPID_D",
    "PRESPID_MAX",
    "PRESPID_MIN",
    "PEEPPID_P",
    "PEEPPID_I",
    "PEEPPID_D",
    "PEEPPID_MAX",
    "PEEPPID_MIN"
};
const MemberInfo PIDParamsMessage::MEMBER_INFO[PIDParamsMessage::MEMBER_NUM]{
    { 0, DataType::FLOAT },
    { 4, DataType::FLOAT },
    { 8, DataType::FLOAT },
    { 12, DataType::FLOAT },
    { 16, DataType::FLOAT },
    { 20, DataType::FLOAT },
    { 24, DataType::FLOAT },
    { 28, DataType::FLOAT },
    { 32, DataType::FLOAT },
    { 36, DataType::FLOAT },
    { 40, DataType::FLOAT },
    { 44, DataType::FLOAT },
    { 48, DataType::FLOAT },
    { 52, DataType::FLOAT },
    { 56, DataType::FLOAT }
};

QString PIDParamsMessage::format() const
{
    return QString(";%1;%2;%3;%4;%5;%6;%7;%8;%9;%10;%11;%12;%13;%14;%15;%16\n")
        .arg(PIDParamsMessage::MSG_ID)
        .arg(MOTPID_P())
        .arg(MOTPID_I())
        .arg(MOTPID_D())
        .arg(MOTPID_MAX())
        .arg(MOTPID_MIN())
        .arg(PRESPID_P())
        .arg(PRESPID_I())
        .arg(PRESPID_D())
        .arg(PRESPID_MAX())
        .arg(PRESPID_MIN())
        .arg(PEEPPID_P())
        .arg(PEEPPID_I())
        .arg(PEEPPID_D())
        .arg(PEEPPID_MAX())
        .arg(PEEPPID_MIN());
}
