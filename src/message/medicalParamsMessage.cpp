#include "message/medicalParamsMessage.h"

const uint8_t MedicalParamsMessage::MSG_ID = 2;
const uint8_t MedicalParamsMessage::SEND_MSG_ID = 102;
const uint8_t MedicalParamsMessage::SIZE = 32;

MedicalParamsMessage::MedicalParamsMessage(const QByteArray& data)
{
    const char* ptr = data.data();

    PEEP = *((float*)(ptr + 4));
    targetFiFO2 = *((float*)(ptr + 8));
    PIP = *((float*)(ptr + 12));
    IER = *((float*)(ptr + 16));
    breathPerMin = *((float*)(ptr + 20));
    targetVolume = *((float*)(ptr + 24));
    mode = *((uint8_t*)(ptr + 28));
}

MedicalParamsMessage::MedicalParamsMessage(float PEEP, float targetFiFO2, float PIP, float IER, float breathPerMin, float targetVolume, uint8_t mode)
    : PEEP(PEEP)
    , targetFiFO2(targetFiFO2)
    , PIP(PIP)
    , IER(IER)
    , breathPerMin(breathPerMin)
    , targetVolume(targetVolume)
    , mode(mode)
{
}

QString MedicalParamsMessage::format() const
{
    return QString(";%1;%2;%3;%4;%5;%6;%7;%8\n")
        .arg(MedicalParamsMessage::MSG_ID)
        .arg(PEEP)
        .arg(targetFiFO2)
        .arg(PIP)
        .arg(IER)
        .arg(breathPerMin)
        .arg(targetVolume)
        .arg(static_cast<uint16_t>(mode));
}

QByteArray MedicalParamsMessage::getData() const
{
    QByteArray data(SIZE, 0);
    char* ptr = data.data();

    // header
    *((uint8_t*)(ptr)) = START_MARKER_ARRAY[0];
    *((uint8_t*)(ptr + 1)) = START_MARKER_ARRAY[1];
    *((uint8_t*)(ptr + 2)) = SEND_MSG_ID;
    *((uint8_t*)(ptr + 3)) = SIZE;

    // data
    *((float*)(ptr + 4)) = PEEP;
    *((float*)(ptr + 8)) = targetFiFO2;
    *((float*)(ptr + 12)) = PIP;
    *((float*)(ptr + 16)) = IER;
    *((float*)(ptr + 20)) = breathPerMin;
    *((float*)(ptr + 24)) = targetVolume;
    *((uint8_t*)(ptr + 28)) = mode;

    // footer
    const uint8_t payloadSize = SIZE - BaseMessage::HEADER_SIZE - BaseMessage::FOOTER_SIZE;
    *((uint16_t*)(ptr + 29)) = qChecksum((char*)(ptr + BaseMessage::HEADER_SIZE), payloadSize, Qt::ChecksumType::ChecksumIso3309);

    *((uint8_t*)(ptr + 31)) = END_BYTE;

    return data;
}

QString MedicalParamsMessage::formatMode() const
{
    switch (mode) {
    case 0:
        return "idle";
    case 1:
        return "CPAP";
    case 2:
        return "timedBiPAP";
    case 3:
        return "sensBiPAP";
    default:
        return "error";
    }
}
