#include "message/flowCalibrationMessage.h"

const uint8_t FlowCalibrationMessage::MSG_ID = 4;
const uint8_t FlowCalibrationMessage::SEND_MSG_ID = 104;
const uint8_t FlowCalibrationMessage::SIZE = 27;
const uint8_t FlowCalibrationMessage::MEMBER_NUM = 5;
const char* FlowCalibrationMessage::FIELD_NAMES[MEMBER_NUM] = {
    "C0",
    "C1",
    "C2",
    "C3",
    "C4"
};
const MemberInfo FlowCalibrationMessage::MEMBER_INFO[FlowCalibrationMessage::MEMBER_NUM]{
    { 0, DataType::FLOAT },
    { 4, DataType::FLOAT },
    { 8, DataType::FLOAT },
    { 12, DataType::FLOAT },
    { 16, DataType::FLOAT }
};

QString FlowCalibrationMessage::format() const
{
    return QString(";%1;%2;%3;%4;%5;%6\n")
        .arg(FlowCalibrationMessage::MSG_ID)
        .arg(C0())
        .arg(C1())
        .arg(C2())
        .arg(C3())
        .arg(C4());
}
