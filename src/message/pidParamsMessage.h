#ifndef PIDPARAMSMESSAGE_H
#define PIDPARAMSMESSAGE_H

#include "message/annotatedMessage.h"

struct PIDParamsMessage : public AnnotatedMessage {
public:
    const static uint8_t SIZE;
    const static uint8_t MSG_ID;
    const static uint8_t SEND_MSG_ID;
    const static uint8_t MEMBER_NUM;
    static const char* FIELD_NAMES[];
    static const MemberInfo MEMBER_INFO[];

    using AnnotatedMessage::AnnotatedMessage;

    QString format() const final;

    QSharedPointer<AnnotatedMessage> clone() const override
    {
        return QSharedPointer<PIDParamsMessage>::create(getPayload());
    }

    uint8_t getSize() const final { return PIDParamsMessage::SIZE; }
    uint8_t getId() const final { return PIDParamsMessage::MSG_ID; }
    uint8_t getSendId() const final { return PIDParamsMessage::SEND_MSG_ID; }
    uint8_t getMemberNum() const final { return PIDParamsMessage::MEMBER_NUM; }
    const char** getFieldNames() const final { return PIDParamsMessage::FIELD_NAMES; }
    const MemberInfo* getMemberInfo() const final { return PIDParamsMessage::MEMBER_INFO; }

    float MOTPID_P() const { return *(float*)(data.data() + 0); }
    float MOTPID_I() const { return *(float*)(data.data() + 4); }
    float MOTPID_D() const { return *(float*)(data.data() + 8); }
    float MOTPID_MAX() const { return *(float*)(data.data() + 12); }
    float MOTPID_MIN() const { return *(float*)(data.data() + 16); }
    float PRESPID_P() const { return *(float*)(data.data() + 20); }
    float PRESPID_I() const { return *(float*)(data.data() + 24); }
    float PRESPID_D() const { return *(float*)(data.data() + 28); }
    float PRESPID_MAX() const { return *(float*)(data.data() + 32); }
    float PRESPID_MIN() const { return *(float*)(data.data() + 36); }
    float PEEPPID_P() const { return *(float*)(data.data() + 40); }
    float PEEPPID_I() const { return *(float*)(data.data() + 44); }
    float PEEPPID_D() const { return *(float*)(data.data() + 48); }
    float PEEPPID_MAX() const { return *(float*)(data.data() + 52); }
    float PEEPPID_MIN() const { return *(float*)(data.data() + 56); }
};

#endif // PIDPARAMSMESSAGE_H
