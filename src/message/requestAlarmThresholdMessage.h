#ifndef REQUESTALARMTHRESHOLDMESSAGE_H
#define REQUESTALARMTHRESHOLDMESSAGE_H

#include "message/baseMessage.h"

class RequestAlarmThresholdMessage : public BaseMessage {
public:
    static const uint8_t SIZE;
    static const uint8_t MSG_ID;

    RequestAlarmThresholdMessage(uint8_t alarm, uint8_t detail)
        : alarmNo(alarm)
        , detailNo(detail)
    {
    }

    QByteArray getData() const;

private:
    uint8_t alarmNo;
    uint8_t detailNo;
};

#endif // REQUESTALARMTHRESHOLDMESSAGE_H
