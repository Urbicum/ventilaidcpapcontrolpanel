#include "errorMessage.h"

#include <QByteArray>

const uint8_t ErrorMessage::MSG_ID = 99;
const uint8_t ErrorMessage::SIZE = 8;

ErrorMessage::ErrorMessage(const QByteArray& data)
{
    const char* ptr = data.data();

    errorId = *((uint8_t*)(ptr));
}

uint8_t ErrorMessage::getErrorId() const
{
    return errorId;
}
