#ifndef FLOWCALIBRATIONMESSAGE_H
#define FLOWCALIBRATIONMESSAGE_H

#include "message/annotatedMessage.h"

struct FlowCalibrationMessage : public AnnotatedMessage {
public:
    const static uint8_t SIZE;
    const static uint8_t MSG_ID;
    const static uint8_t SEND_MSG_ID;
    const static uint8_t MEMBER_NUM;
    static const char* FIELD_NAMES[];
    static const MemberInfo MEMBER_INFO[];

    using AnnotatedMessage::AnnotatedMessage;

    QString format() const final;

    QSharedPointer<AnnotatedMessage> clone() const override
    {
        return QSharedPointer<FlowCalibrationMessage>::create(getPayload());
    }

    uint8_t getSize() const final { return FlowCalibrationMessage::SIZE; }
    uint8_t getId() const final { return FlowCalibrationMessage::MSG_ID; }
    uint8_t getSendId() const final { return FlowCalibrationMessage::SEND_MSG_ID; }
    uint8_t getMemberNum() const final { return FlowCalibrationMessage::MEMBER_NUM; }
    const char** getFieldNames() const final { return FlowCalibrationMessage::FIELD_NAMES; }
    const MemberInfo* getMemberInfo() const final { return FlowCalibrationMessage::MEMBER_INFO; }

    float C0() const { return *(float*)(data.data() + 0); }
    float C1() const { return *(float*)(data.data() + 4); }
    float C2() const { return *(float*)(data.data() + 8); }
    float C3() const { return *(float*)(data.data() + 12); }
    float C4() const { return *(float*)(data.data() + 16); }
};

#endif // FLOWCALIBRATIONMESSAGE_H
