#include "message/alarmThresholdMessage.h"

const uint8_t AlarmThresholdMessage::MSG_ID = 30;
const uint8_t AlarmThresholdMessage::SEND_MSG_ID = 130;
const uint8_t AlarmThresholdMessage::SIZE = 25;

AlarmThresholdMessage::AlarmThresholdMessage(const QByteArray& data)
{
    const char* ptr = data.data();

    alarmLowValue = *((float*)(ptr + 4));
    warnLowValue = *((float*)(ptr + 8));
    warnHighValue = *((float*)(ptr + 12));
    alarmHighValue = *((float*)(ptr + 16));
    alarmNo = *((uint8_t*)(ptr + 20));
    detailNo = *((uint8_t*)(ptr + 21));
}

AlarmThresholdMessage::AlarmThresholdMessage(float alarmLow, float warnLow, float warnHigh, float alarmHigh, uint8_t alarm, uint8_t detail)
    : alarmLowValue(alarmLow)
    , warnLowValue(warnLow)
    , warnHighValue(warnHigh)
    , alarmHighValue(alarmHigh)
    , alarmNo(alarm)
    , detailNo(detail)
{
}

QString AlarmThresholdMessage::format() const
{
    return QString(";%1;%2;%3;%4;%5;%6;%7\n")
        .arg(AlarmThresholdMessage::MSG_ID)
        .arg(alarmLowValue)
        .arg(warnLowValue)
        .arg(warnHighValue)
        .arg(alarmHighValue)
        .arg(static_cast<uint16_t>(alarmNo))
        .arg(static_cast<uint16_t>(detailNo));
}

QByteArray AlarmThresholdMessage::getData() const
{
    QByteArray data(SIZE, 0);
    char* ptr = data.data();

    // header
    *((uint8_t*)(ptr)) = START_MARKER_ARRAY[0];
    *((uint8_t*)(ptr + 1)) = START_MARKER_ARRAY[1];
    *((uint8_t*)(ptr + 2)) = SEND_MSG_ID;
    *((uint8_t*)(ptr + 3)) = SIZE;

    // data
    *((float*)(ptr + 4)) = alarmLowValue;
    *((float*)(ptr + 8)) = warnLowValue;
    *((float*)(ptr + 12)) = warnHighValue;
    *((float*)(ptr + 16)) = alarmHighValue;
    *((uint8_t*)(ptr + 20)) = alarmNo;
    *((uint8_t*)(ptr + 21)) = detailNo;

    // footer
    const uint8_t payloadSize = SIZE - BaseMessage::HEADER_SIZE - BaseMessage::FOOTER_SIZE;
    *((uint16_t*)(ptr + 22)) = qChecksum((char*)(ptr + BaseMessage::HEADER_SIZE), payloadSize, Qt::ChecksumType::ChecksumIso3309);

    *((uint8_t*)(ptr + 24)) = END_BYTE;

    return data;
}
