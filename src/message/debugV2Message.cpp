#include "message/debugV2Message.h"

const uint8_t DebugV2Message::MSG_ID = 80;
const uint8_t DebugV2Message::SIZE = 55;

DebugV2Message::DebugV2Message(const QByteArray& data)
{
    const char* ptr = data.data();

    pressure0 = *((float*)(ptr + 4));
    pressure1 = *((float*)(ptr + 8));
    pressure2 = *((float*)(ptr + 12));
    pressure3 = *((float*)(ptr + 16));
    motorOut = *((float*)(ptr + 20));
    motorRPM = *((float*)(ptr + 24));
    motorCurr = *((float*)(ptr + 28));
    motorTemp = *((float*)(ptr + 32));
    deviceTemp = *((float*)(ptr + 36));
    ambientTemp = *((float*)(ptr + 40));
    ambientHumidity = *((float*)(ptr + 44));
    peepOut = *((float*)(ptr + 48));
}

QString DebugV2Message::format() const
{
    return QString(";%1;%2;%3;%4;%5;%6;%7;%8;%9;%10;%11;%12;%13\n")
        .arg(DebugV2Message::MSG_ID)
        .arg(pressure0)
        .arg(pressure1)
        .arg(pressure2)
        .arg(pressure3)
        .arg(motorOut)
        .arg(motorRPM)
        .arg(motorCurr)
        .arg(motorTemp)
        .arg(deviceTemp)
        .arg(ambientTemp)
        .arg(ambientHumidity)
        .arg(peepOut);
}
