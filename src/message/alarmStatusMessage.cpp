#include "message/alarmStatusMessage.h"

const uint8_t AlarmStatusMessage::MSG_ID = 10;
const uint8_t AlarmStatusMessage::SIZE = 11;

AlarmStatusMessage::AlarmStatusMessage(const QByteArray& data)
{
    const char* ptr = data.data();

    code = *((uint32_t*)(ptr + 4));
}

QString AlarmStatusMessage::format() const
{
    return QString(";%1;%2\n")
        .arg(AlarmStatusMessage::MSG_ID)
        .arg(code);
}
