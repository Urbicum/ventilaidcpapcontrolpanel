#ifndef BREATHANALYSERMESSAGE_H
#define BREATHANALYSERMESSAGE_H

#include "message/baseMessage.h"

struct BreathAnalyserMessage : public BaseMessage {
public:
    const static uint8_t SIZE;
    const static uint8_t MSG_ID;

    BreathAnalyserMessage(const QByteArray& data);

    QString format() const final;

    float PTrig;
    float VTrig;
    float ESens;
    float ISens;
};

#endif // BREATHANALYSERMESSAGE_H
