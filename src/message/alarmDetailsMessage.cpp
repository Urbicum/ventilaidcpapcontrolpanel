#include "message/alarmDetailsMessage.h"

const uint8_t AlarmDetailsMessage::MSG_ID = 11;
const uint8_t AlarmDetailsMessage::SIZE = 15;

AlarmDetailsMessage::AlarmDetailsMessage(const QByteArray& data)
{
    const char* ptr = data.data();

    code = *((uint32_t*)(ptr + 4));
    detail = *((uint32_t*)(ptr + 8));
}

QString AlarmDetailsMessage::format() const
{
    return QString(";%1;%2;%3\n")
        .arg(AlarmDetailsMessage::MSG_ID)
        .arg(code)
        .arg(detail);
}
