#include "debugDialog.h"

#include "message/debugV2Message.h"

#include "ui_debugDialog.h"

DebugDialog::DebugDialog(QWidget* parent)
    : QDialog(parent)
    , ui(new Ui::DebugDialog)
{
    ui->setupUi(this);

    ui->motorCurrent->setup("Motor current:", "%1 A");
    ui->motorTemp->setup("Motor tempt:", "%1 C");
    ui->deviceTemp->setup("Device temp:", "%1 C");
    ui->ambientTemp->setup("Ambient temp:", "%1 C");
    ui->ambientHumidity->setup("Ambient humidity:", "%1 %RH");
    ui->PEEPOut->setup("PEEP out:", "%1 us");

    ui->pressure0->setupDebugChart("pressure0", "Pressure 0");
    ui->pressure1->setupDebugChart("pressure1", "Pressure 1");
    ui->pressure2->setupDebugChart("pressure2", "Pressure 2");
    ui->pressure3->setupDebugChart("pressure3", "Pressure 3");
    ui->motorOut->setupDebugChart("motorOut", "Motor out");
    ui->motorRPM->setupDebugChart("motorRPM", "Motor RPM");
}

DebugDialog::~DebugDialog()
{
    delete ui;
}

void DebugDialog::clear()
{
    ui->motorCurrent->clear();
    ui->motorTemp->clear();
    ui->deviceTemp->clear();
    ui->ambientTemp->clear();
    ui->ambientHumidity->clear();
    ui->PEEPOut->clear();

    ui->pressure0->clear();
    ui->pressure1->clear();
    ui->pressure2->clear();
    ui->pressure3->clear();
    ui->motorOut->clear();
    ui->motorRPM->clear();
}

void DebugDialog::onNewGenericMessage(QSharedPointer<const BaseMessage> msg)
{
    auto debugMsg = qSharedPointerDynamicCast<const DebugV2Message>(msg);
    if (debugMsg) {
        ui->motorCurrent->setValue(debugMsg->motorCurr);
        ui->motorTemp->setValue(debugMsg->motorTemp);
        ui->deviceTemp->setValue(debugMsg->deviceTemp);
        ui->ambientTemp->setValue(debugMsg->ambientTemp);
        ui->ambientHumidity->setValue(debugMsg->ambientHumidity);
        ui->PEEPOut->setValue(debugMsg->peepOut);

        ui->pressure0->addValue(debugMsg->pressure0);
        ui->pressure1->addValue(debugMsg->pressure1);
        ui->pressure2->addValue(debugMsg->pressure2);
        ui->pressure3->addValue(debugMsg->pressure3);
        ui->motorOut->addValue(debugMsg->motorOut);
        ui->motorRPM->addValue(debugMsg->motorRPM);
    }
}
