#include "setDialog.h"
#include "ui_setDialog.h"

#include <QSettings>

SetDialog::SetDialog(QString name, float value, QString settingsKey, QString unit, float sliderFactor)
    : QDialog(nullptr)
    , sliderFactor(sliderFactor)
    , ui(new Ui::SetDialog)
{
    ui->setupUi(this);

    setWindowTitle(name);

    QSettings settings;
    settings.beginGroup(settingsKey);

    ui->slider->setRange(settings.value("min").toFloat() * sliderFactor, settings.value("max").toFloat() * sliderFactor);
    ui->slider->setValue(value * sliderFactor);

    ui->value->setText(unit.arg(value));

    connect(ui->slider, &QSlider::valueChanged, [this, unit](int value) {
        ui->value->setText(unit.arg(value / this->sliderFactor, 4, 'f', 1));
    });
}

SetDialog::~SetDialog()
{
    delete ui;
}

float SetDialog::getValue() const
{
    return ui->slider->value() / sliderFactor;
}
