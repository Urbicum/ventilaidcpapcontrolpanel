#ifndef SERIALOVERSEER_H
#define SERIALOVERSEER_H

#include <QObject>

#include "message/requestMessage.h"
#include "messageHandler.h"
#include "serial.h"
#include "utils.h"

class RuntimeConfig;

class SerialOverseer : public QObject {
    Q_OBJECT
public:
    enum class State {
        Normal,
        Reconnecting
    };

    explicit SerialOverseer(QObject* parent, Utils::Target target, RuntimeConfig* runtimeConfig);
    virtual ~SerialOverseer();

    bool isConnected();
    void disconnect();
    bool isReconnecting();
    void stopReconnecting();

public slots:
    void sendMessage(StatusMessage msg);
    void sendMessage(RequestMessage msg);
    void sendChangeParamsMessage(QSharedPointer<const AnnotatedMessage> msg);
    void open(QString portName, Serial::BaudRate baudRate);
    void reopenLastSerial();

signals:
    void connected(QString portName);
    void disconnected(Serial::DisconnectionReason reason, QString msg);
    void reconnectingStarted(QString message);
    void newStatusMessage(StatusMessage msg);
    void newDebugMessage(DebugMessage msg);
    void statusMessageRequest();
    void newGenericMessage(QSharedPointer<const BaseMessage> msg);
    void sendNewGenericMessage(QSharedPointer<const BaseMessage> msg);
    void newChangeParamsMessage(QSharedPointer<const AnnotatedMessage> msg);

private:
    Serial* serial;
    QTimer* sendTimer;
    QTimer* reconnectTimer;
    State state;
    MessageHandler* messageHandler;
    QString lastSerial;
    Serial::BaudRate lastBaudRate;
};

#endif // SERIALOVERSEER_H
