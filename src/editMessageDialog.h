#ifndef EDITMESSAGEDIALOG_H
#define EDITMESSAGEDIALOG_H

#include "message/annotatedMessage.h"

#include <QDialog>

class QAbstractSpinBox;

namespace Ui {
class EditMessageDialog;
}

class EditMessageDialog : public QDialog {
    Q_OBJECT

public:
    explicit EditMessageDialog(QWidget* parent, QSharedPointer<const AnnotatedMessage> formatData);
    ~EditMessageDialog();

    QSharedPointer<AnnotatedMessage> getMessage() const;

private:
    QAbstractSpinBox* buildWidget(const DataIterator::return_type& iterator);

    Ui::EditMessageDialog* ui;
    QSharedPointer<AnnotatedMessage> tmpMsg;
    std::vector<QAbstractSpinBox*> valueWidgets;
};

#endif // EDITMESSAGEDIALOG_H
