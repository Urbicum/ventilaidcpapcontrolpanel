#ifndef SERIAL_H
#define SERIAL_H

#include <QObject>
#include <QtSerialPort>

#include "message/alertMessage.h"
#include "message/annotatedMessage.h"
#include "message/debugMessage.h"
#include "message/errorMessage.h"
#include "message/mainParamsMessage.h"
#include "message/medicalParamsMessage.h"
#include "message/requestMessage.h"
#include "message/statusMessage.h"

class Serial : public QObject {
    Q_OBJECT

public:
    enum BaudRate {
        Baud1200 = 1200,
        Baud2400 = 2400,
        Baud4800 = 4800,
        Baud9600 = 9600,
        Baud19200 = 19200,
        Baud38400 = 38400,
        Baud57600 = 57600,
        Baud115200 = 115200,
        Baud230400 = 230400,
        Baud460800 = 460800,
        UnknownBaud = -1
    };

    enum class DisconnectionReason {
        UserRequest,
        Error
    };

    explicit Serial(QObject* parent, bool skipCrc);
    virtual ~Serial();

    bool isConnected() const;

    void disconnect();

signals:
    void connected(QString port);
    void disconnected(DisconnectionReason reason, QString message);
    void newStatusMessage(StatusMessage msg);
    void newDebugMessage(DebugMessage msg);
    void newErrorMessage(ErrorMessage msg);
    void newAlertMessage(AlertMessage msg);
    void newGenericMessage(QSharedPointer<const BaseMessage> msg);
    void newChangeParamsMessage(QSharedPointer<const AnnotatedMessage> msg);

public slots:
    void sendMessage(StatusMessage msg);
    void sendMessage(RequestMessage msg);
    void sendChangeParamsMessage(QSharedPointer<const AnnotatedMessage> msg);
    void open(QString portName, Serial::BaudRate baudRate);
    void onSendNewGenericMessage(QSharedPointer<const BaseMessage> msg);

private slots:
    void close(DisconnectionReason reason, QString messsage);
    void handleReadyRead();
    void handleTimeout();
    void handleError(QSerialPort::SerialPortError error);

private:
    bool parseMessage();
    bool parseMessageImpl(uint8_t msgId, uint8_t msgSize, uint8_t payloadSize);

    bool sizeCheck(const QMap<uint8_t, uint8_t>& expectedMessages, uint8_t msgId, uint8_t msgSize);

private:
    const size_t TIMEOUT_MS;

    QSerialPort* serialPort;
    QTimer* watchdog;
    bool _connected;
    QByteArray dataBuffer;
    bool skipCrc;
};

#endif // SERIAL_H
