#ifndef PORTSELECTIONDIALOG_H
#define PORTSELECTIONDIALOG_H

#include "serial.h"

#include <QDialog>

namespace Ui {
class PortSelectionDialog;
}

class PortSelectionDialog : public QDialog {
    Q_OBJECT

public:
    explicit PortSelectionDialog(QWidget* parent = nullptr);
    ~PortSelectionDialog();

    Serial::BaudRate getBaudRate() const;
    QString getSeriaPortName() const;

private:
    Ui::PortSelectionDialog* ui;
};

#endif // PORTSELECTIONDIALOG_H
