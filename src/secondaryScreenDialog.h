#ifndef SECONDARYSCREENDIALOG_H
#define SECONDARYSCREENDIALOG_H

#include <QDialog>

namespace Ui {
class SecondaryScreenDialog;
}

class SecondaryScreenDialog : public QDialog {
    Q_OBJECT

public:
    explicit SecondaryScreenDialog(QWidget* parent = nullptr);
    ~SecondaryScreenDialog();

    void setRotTarget(float value);
    void setup();
    void clear();

private:
    Ui::SecondaryScreenDialog* ui;
};

#endif // SEONDARYSCREENWIDGET_H
