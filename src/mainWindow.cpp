#include "mainWindow.h"
#include "ui_mainWindow.h"

#include "aboutDialog.h"
#include "debugDialog.h"
#include "editMessageDialog.h"
#include "message/pidParamsMessage.h"
#include "portSelectionDialog.h"
#include "secondaryScreenDialog.h"
#include "sendMessageDialog.h"
#include "serialOverseer.h"
#include "setDialog.h"
#include "settingsDialog.h"
#include "settingsScreenDialog.h"
#include "utils.h"

#include <QDebug>
#include <QInputDialog>
#include <QMessageBox>
#include <QProgressDialog>

MainWindow::MainWindow(QWidget* parent, Utils::Target target, SerialOverseer* serial)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , target(target)
    , progressDialog(nullptr)
    , debugDialog(nullptr)
    , secondaryScreenDialog(nullptr)
    , serialOverseer(serial)
    , settingsScreenDialog(nullptr)
{
    ui->setupUi(this);

    qApp->setWindowIcon(QIcon(":/resource/icon_1024x1024.png"));

    connect(serialOverseer, &SerialOverseer::connected, [this](QString portName) { onConnectionStausChanged(true, portName); });

    connect(serialOverseer, &SerialOverseer::disconnected, [this](Serial::DisconnectionReason reason, QString msg) {
        onConnectionStausChanged(false, "");

        // Report errors
        if (reason == Serial::DisconnectionReason::Error) {
            statusBar()->showMessage(msg);

            if (serialOverseer->isReconnecting())
                this->progressDialog->setLabelText(QString("Connect failed with: %1").arg(msg));
        }
    });

    connect(serialOverseer, &SerialOverseer::reconnectingStarted, [this](QString msg) {
        progressDialog = new QProgressDialog(QString("Connect failed with: %1").arg(msg), "Cancel", 0, 0, this);
        progressDialog->setWindowModality(Qt::WindowModal);
        progressDialog->setWindowTitle("Reconnecting");

        if (this->target == Utils::Target::Raspberry)
            progressDialog->setCancelButton(nullptr);

        connect(serialOverseer, &SerialOverseer::connected, progressDialog, [this]() {
            progressDialog->cancel();
            serialOverseer->stopReconnecting();
            progressDialog->deleteLater();
            progressDialog = nullptr;
        });
        connect(progressDialog, &QProgressDialog::canceled, [this]() {
            qDebug() << "Reconnection canceled";

            serialOverseer->stopReconnecting();
            progressDialog->deleteLater();
            progressDialog = nullptr;

            if (this->target == Utils::Target::Raspberry)
                this->serialOverseer->reopenLastSerial();
        });

        progressDialog->show();
    });

    connect(serialOverseer, &SerialOverseer::newGenericMessage, this, &MainWindow::onNewGenericMessage);

    if (this->target == Utils::Target::Raspberry) {
        statusBar()->hide();
        menuBar()->hide();
        QApplication::setOverrideCursor(Qt::BlankCursor);
        showFullScreen();
    } else {
        debugDialog = new DebugDialog(this);
        secondaryScreenDialog = new SecondaryScreenDialog(this);
        settingsScreenDialog = new SettingsScreenDialog(this);

        connect(serialOverseer, &SerialOverseer::newGenericMessage, debugDialog, &DebugDialog::onNewGenericMessage);
        connect(serialOverseer, &SerialOverseer::newGenericMessage, settingsScreenDialog, &SettingsScreenDialog::onNewGenericMessage);

        connect(serialOverseer, &SerialOverseer::statusMessageRequest, settingsScreenDialog, &SettingsScreenDialog::onRequestNewMessage);
        connect(settingsScreenDialog, &SettingsScreenDialog::sendNewGenericMessage, serialOverseer, &SerialOverseer::sendNewGenericMessage);
    }

    connect(serialOverseer, &SerialOverseer::newChangeParamsMessage, [this](QSharedPointer<const AnnotatedMessage> msg) {
        this->progressDialog->cancel();
        this->progressDialog->deleteLater();
        this->progressDialog = nullptr;

        editDialog = new EditMessageDialog(this, msg);
        editDialog->show();

        connect(this->editDialog, &EditMessageDialog::accepted, [this]() {
            auto sendMessage = this->editDialog->getMessage();
            if (this->serialOverseer->isConnected())
                this->serialOverseer->sendChangeParamsMessage(sendMessage);
            this->editDialog->deleteLater();
            this->editDialog = nullptr;
        });

        connect(this->editDialog, &EditMessageDialog::rejected, [this]() {
            this->editDialog->deleteLater();
            this->editDialog = nullptr;
        });
    });

    setupMenu();
    setupCharts();
    setupValueWidgets();

    onConnectionStausChanged(false, "");

    // without this mainwindow layout breaks on Raspberry Pi
    adjustSize();
}

MainWindow::~MainWindow()
{
    qDebug() << "~MainWindow";
    delete ui;
}

void MainWindow::onConnectionStausChanged(bool connected, QString portName)
{
    ui->actionSendMessage->setEnabled(connected);
    ui->actionSettings->setEnabled(!connected);
    ui->actionShowDebug->setEnabled(connected);
    ui->actionShowSecondary->setEnabled(connected);
    ui->actionShowSettings->setEnabled(connected);

    if (connected) {
        ui->actionConnection->setText("Disconnect");
        statusBar()->showMessage("Connected to: " + portName);

        if (this->target == Utils::Target::Desktop) {
            QSettings settings;

            settings.beginGroup("debugScreen");
            if (settings.contains("geometry"))
                debugDialog->restoreGeometry(settings.value("geometry").toByteArray());
            settings.endGroup();

            settings.beginGroup("secondaryScreen");
            if (settings.contains("geometry"))
                secondaryScreenDialog->restoreGeometry(settings.value("geometry").toByteArray());
            settings.endGroup();

            settings.beginGroup("settingsScreen");
            if (settings.contains("geometry"))
                settingsScreenDialog->restoreGeometry(settings.value("geometry").toByteArray());
            settings.endGroup();

            debugDialog->setVisible(ui->actionShowDebug->isChecked());
            secondaryScreenDialog->setVisible(ui->actionShowSecondary->isChecked());
            settingsScreenDialog->setVisible(ui->actionShowSettings->isChecked());
        }
    } else {
        ui->actionConnection->setText("Connect");
        statusBar()->showMessage("Not connected");
    }

    if (!connected) {
        ui->pressureTrendChart->clear();
        ui->flowTrendChart->clear();

        if (this->target == Utils::Target::Desktop) {
            QSettings settings;

            settings.beginGroup("debugScreen");
            settings.setValue("geometry", debugDialog->saveGeometry());
            settings.endGroup();

            settings.beginGroup("secondaryScreen");
            settings.setValue("geometry", secondaryScreenDialog->saveGeometry());
            settings.endGroup();

            settings.beginGroup("settingsScreen");
            settings.setValue("geometry", settingsScreenDialog->saveGeometry());
            settings.endGroup();

            debugDialog->clear();
            secondaryScreenDialog->clear();
            settingsScreenDialog->clear();

            debugDialog->hide();
            secondaryScreenDialog->hide();
            settingsScreenDialog->hide();
        }
    }
}

void MainWindow::onNewGenericMessage(QSharedPointer<const BaseMessage> msg)
{
    auto paramsMsg = qSharedPointerDynamicCast<const MainParamsMessage>(msg);
    if (paramsMsg) {
        ui->pressureTrendChart->addValue(paramsMsg->pressure);
        ui->flowTrendChart->addValue(paramsMsg->flow);

        ui->inhalePressureMean->setValue(paramsMsg->inhalePressureMean);
        ui->exhalePressureMean->setValue(paramsMsg->exhalePressureMean);
        ui->inhaleVol->setValue(paramsMsg->inhaleVol);
        ui->inhaleVolPerMin->setValue(paramsMsg->inhaleVolPerMin);
        ui->exhaleVol->setValue(paramsMsg->exhaleVol);
        // TODO: ui->bloodOxygen->setValue();
        ui->breathPerMin->setValue(paramsMsg->breathPerMin);
        // TODO: ui->heartRate->setValue();
        ui->breathPhase->setValue(paramsMsg->formatBreathPhase());
        ui->lastInhaleType->setValue(paramsMsg->formatInhaleType());
        ui->lungCompl->setValue(paramsMsg->lungCompl);

        secondaryScreenDialog->setRotTarget(paramsMsg->rotTarget);

        return;
    }
}

void MainWindow::setupMenu()
{
    connect(ui->actionConnection, &QAction::triggered, [this]() {
        if (this->serialOverseer->isConnected())
            this->serialOverseer->disconnect();
        else {
            bool isLogDirValid = Utils::isPathWritable(QSettings().value("loggingDirPath").toString());
            if (isLogDirValid)
                this->openUserSelectedSerial();
            else {
                QMessageBox msgBox(QMessageBox::Icon::Warning, "Connection error", "Please set correct logging directory before connecting");
                msgBox.exec();

                SettingsDialog dialog;
                dialog.exec();
            }
        }
    });

    connect(ui->actionSendMessage, &QAction::triggered, [this]() {
        SendRequestMessageDialog dialog;
        auto result = dialog.exec();
        if (result == QDialog::Accepted && serialOverseer->isConnected()) {
            serialOverseer->sendMessage(dialog.getMessage());

            progressDialog = new QProgressDialog(QString("Waiting for response"), "Cancel", 0, 0, this);
            progressDialog->setWindowModality(Qt::WindowModal);
            progressDialog->setWindowTitle("Waiting");

            progressDialog->show();
        } else {
            QMessageBox msgBox(QMessageBox::Icon::Warning, "Connection error", "Please try again");
        }
    });

    connect(ui->actionSettings, &QAction::triggered, []() {
        SettingsDialog dialog;
        dialog.exec();
    });

    connect(ui->actionAbout, &QAction::triggered, []() {
        AboutDialog dialog;
        dialog.exec();
    });

    if (this->target == Utils::Target::Desktop) {
        connect(ui->actionShowDebug, &QAction::toggled, debugDialog, &DebugDialog::setVisible);
        connect(ui->actionShowSecondary, &QAction::toggled, secondaryScreenDialog, &SecondaryScreenDialog::setVisible);
        connect(ui->actionShowSettings, &QAction::toggled, settingsScreenDialog, &SecondaryScreenDialog::setVisible);
    }
}

void MainWindow::setupCharts()
{
    const QVector<std::tuple<ChartWidget*, QString, QString>> CHARTS = {
        std::make_tuple(ui->pressureTrendChart, "pressureTrend", "%1 cmH20"),
        std::make_tuple(ui->flowTrendChart, "flowTrend", "%1 L/min"),
    };

    for (const auto& tuple : CHARTS) {
        ChartWidget* chart;
        QString settingsKey, unitTemplate;
        std::tie(chart, settingsKey, unitTemplate) = tuple;

        chart->setup(settingsKey, unitTemplate);
    }
}

void MainWindow::setupValueWidgets()
{
    ui->inhalePressureMean->setup("Inhale pressure mean:", "%1 cmH20");
    ui->exhalePressureMean->setup("Exhale pressure mean:", "%1 cmH20");
    ui->inhaleVol->setup("Inhale volume:", "%1 mL");
    ui->inhaleVolPerMin->setup("Inhale volume per minute:", "%1 mL/min?");
    ui->exhaleVol->setup("Exhale volume:", "%1 mL");
    ui->bloodOxygen->setup("Blood oxygen:", "%1 %");
    ui->breathPerMin->setup("Breaths per minute:", "%1/min");
    ui->heartRate->setup("Heart rate:", "%1/min");
    ui->breathPhase->setup("Last inhale type:");
    ui->lastInhaleType->setup("Last inhale type:");
    ui->lungCompl->setup("Lung compliance:", "%1 L/cmH20");
}

void MainWindow::openUserSelectedSerial()
{
    if (QSerialPortInfo::availablePorts().empty()) {
        QMessageBox msgBox(QMessageBox::Icon::Critical, "Error", "No serial ports are available");
        msgBox.exec();
        return;
    }

    PortSelectionDialog dialog;
    auto dialogStatus = dialog.exec();
    if (dialogStatus == QDialog::Rejected)
        return;

    serialOverseer->open(dialog.getSeriaPortName(), dialog.getBaudRate());
}
