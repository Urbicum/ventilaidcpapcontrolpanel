#ifndef DEBUGDIALOG_H
#define DEBUGDIALOG_H

#include <QDialog>

class BaseMessage;

namespace Ui {
class DebugDialog;
}

class DebugDialog : public QDialog {
    Q_OBJECT

public:
    explicit DebugDialog(QWidget* parent = nullptr);
    ~DebugDialog();

    void clear();

public slots:
    void onNewGenericMessage(QSharedPointer<const BaseMessage> msg);

private:
    Ui::DebugDialog* ui;
};

#endif // DEBUGDIALOG_H
