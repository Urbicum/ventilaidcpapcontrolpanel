#include "sendMessageDialog.h"
#include "ui_sendMessageDialog.h"

#include "message/flowCalibrationMessage.h"
#include "message/pidParamsMessage.h"
#include "message/pressureSensorCalibMessage.h"
#include "message/workpointCalibrationMessage.h"

#include <QSettings>

SendRequestMessageDialog::SendRequestMessageDialog(QWidget* parent)
    : QDialog(parent)
    , ui(new Ui::SendMessageDialog)
{
    ui->setupUi(this);

    auto* comboBox = ui->comboBox;
    comboBox->addItem("pidSettingsParams", PIDParamsMessage::MSG_ID);
    comboBox->addItem("flowCalibrationParams", FlowCalibrationMessage::MSG_ID);
    comboBox->addItem("workpointCalibrationParams", WorkpointCalibrationMessage::MSG_ID);
    comboBox->addItem("pressureS0Calibration", PressureSensorCalibMessage::MSG_IDS[0]);
    comboBox->addItem("pressureS1Calibration", PressureSensorCalibMessage::MSG_IDS[1]);
    comboBox->addItem("pressureS2Calibration", PressureSensorCalibMessage::MSG_IDS[2]);
    comboBox->addItem("pressureS3Calibration", PressureSensorCalibMessage::MSG_IDS[3]);
}

SendRequestMessageDialog::~SendRequestMessageDialog()
{
    delete ui;
}

RequestMessage SendRequestMessageDialog::getMessage() const
{
    auto value = ui->comboBox->currentData().toInt();
    return RequestMessage(value);
}
