#include "settingsScreenDialog.h"
#include "ui_settingsScreenDialog.h"

#include "message/medicalParamsMessage.h"

#include <QSettings>

SettingsScreenDialog::SettingsScreenDialog(QWidget* parent)
    : QDialog(parent)
    , ui(new Ui::SettingsScreenDialog)
{
    ui->setupUi(this);

    ui->peep->setup(5, 15, "PEEP", "%1 cmH2O", "PEEP");
    ui->fio->setup(21, 70, "FiO2", "%1 %", "FiO2");

    ui->pip->setup(5, 15, "PIP", "%1 cmH2O", "PIP");
    ui->ie->setup(1, 4, "IE", "1/%1", "IE");
    ui->bpm->setup(10, 40, "BPM", "%1/min", "BPM");
    ui->targetVolume->setup(300, 2000, "Target volume", "%1 mL", "targetVolume");

    ui->modeComboBox->addItem("idle");
    ui->modeComboBox->addItem("CPAP");
    ui->modeComboBox->addItem("timedBiPAP");
    ui->modeComboBox->addItem("sensBiPAP");

    QSettings settings;
    settings.beginGroup("mode");
    ui->modeComboBox->setCurrentIndex(settings.value("value").toInt());

    connect(ui->modeComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), [](int idx) {
        Q_UNUSED(idx);
        // TODO: based on idx value disable/enable UI
    });
}

SettingsScreenDialog::~SettingsScreenDialog()
{
    QSettings settings;
    settings.beginGroup("mode");
    settings.setValue("value", ui->modeComboBox->currentIndex());
    settings.endGroup();

    settings.sync();

    delete ui;
}

void SettingsScreenDialog::clear()
{
    ui->peep->clear();
    ui->fio->clear();
    ui->pip->clear();
    ui->ie->clear();
    ui->bpm->clear();
    ui->targetVolume->clear();
}

void SettingsScreenDialog::onNewGenericMessage(QSharedPointer<const BaseMessage> msg)
{
    auto medicalParamsMsg = qSharedPointerDynamicCast<const MedicalParamsMessage>(msg);
    if (medicalParamsMsg) {
        ui->peep->setValue(medicalParamsMsg->PEEP);
        ui->fio->setValue(medicalParamsMsg->targetFiFO2);
        ui->pip->setValue(medicalParamsMsg->PIP);
        ui->ie->setValue(medicalParamsMsg->IER);
        ui->bpm->setValue(medicalParamsMsg->breathPerMin);
        ui->targetVolume->setValue(medicalParamsMsg->targetVolume);
        ui->modeLabel->setText(medicalParamsMsg->formatMode());
    }
}

void SettingsScreenDialog::onRequestNewMessage()
{
    auto msg = QSharedPointer<const MedicalParamsMessage>::create(
        ui->peep->getValue(),
        ui->fio->getValue(),
        ui->pip->getValue(),
        ui->ie->getValue(),
        ui->bpm->getValue(),
        ui->targetVolume->getValue(),
        ui->modeComboBox->currentIndex());
    emit sendNewGenericMessage(msg);
}
