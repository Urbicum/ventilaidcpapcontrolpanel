#include "utils.h"

#include <iostream>

#include <QCoreApplication>
#include <QFileInfo>
#include <QSettings>

void Utils::initApplication()
{
    QCoreApplication::setOrganizationName("VentilAid");
    QCoreApplication::setOrganizationDomain("ventilaid.org");
    QCoreApplication::setApplicationName("VentilAid CPAP Control Panel");
    QCoreApplication::setApplicationVersion(QString(GIT_VERSION));
}

void Utils::initSettingsWithDefult()
{
    const QMap<QString, int> DEFAULT_VALUES_INT = {
        { "serialTimeoutMs", 5000 },
        { "sendIntervalMs", 1000 },
        { "refreshRateHz", 10 },
        { "chartWindowSeconds", 30 },
        { "lastSelectedBaudRate", 115200 },
    };

    const QMap<QString, QString> DEFAULT_VALUES_STRING = {
        { "lastSelectedPort", "" },
        { "loggingDirPath", "" },
    };

    const QMap<QString, bool> DEFAULT_VALUES_BOOL = {
        { "autoReconnect", true },
        { "gatherLogs", true },
    };

    const QMap<QString, int> PRESSURE_TREND_DEFAULT = {
        { "chartWindowSeconds", 20 },
        { "min", -2 },
        { "max", 25 },
    };

    const QMap<QString, int> FLOW_TREND_DEFAULT = {
        { "chartWindowSeconds", 20 },
        { "min", -50 },
        { "max", 50 },
    };

    const QMap<QString, int> PRESSURE_DEFAULT = {
        { "chartWindowSeconds", 30 },
        { "min", 0 },
        { "max", 50 },
    };

    const QMap<QString, int> MOTOR_OUT_DEFAULT = {
        { "chartWindowSeconds", 30 },
        { "min", 0 },
        { "max", 1000 },
    };

    const QMap<QString, int> MOTOR_RPM_DEFAULT = {
        { "chartWindowSeconds", 30 },
        { "min", 0 },
        { "max", 20000 },
    };

    const std::map<QString, QMap<QString, int>> DEFAULT_VALUES_CHARTS = {
        { "pressureTrend", PRESSURE_TREND_DEFAULT },
        { "flowTrend", FLOW_TREND_DEFAULT },
        { "pressure0", PRESSURE_DEFAULT },
        { "pressure1", PRESSURE_DEFAULT },
        { "pressure2", PRESSURE_DEFAULT },
        { "pressure3", PRESSURE_DEFAULT },
        { "motorOut", MOTOR_OUT_DEFAULT },
        { "motorRPM", MOTOR_RPM_DEFAULT },
    };

    QSettings settings;

    std::cout << "Config path: " << settings.fileName().toStdString() << std::endl;

    {
        for (const auto& chart : DEFAULT_VALUES_CHARTS) {
            settings.beginGroup(chart.first);

            auto existingKeys = settings.childKeys();
            for (auto key : chart.second.keys())
                if (!existingKeys.contains(key))
                    settings.setValue(key, chart.second[key]);

            settings.endGroup();
        }
    }

    {
        const QMap<QString, int> DEFAULT_SETTINGS_SCREEN = {
            { "PEEP", 10 },
            { "FiO2", 45 },
            { "mode", 0 },
            { "PIP", 10 },
            { "IE", 3 },
            { "BPM", 25 },
            { "targetVolume", 1150 },
        };

        for (auto key : DEFAULT_SETTINGS_SCREEN.keys()) {
            settings.beginGroup(key);

            auto existingKeys = settings.childKeys();
            if (!existingKeys.contains("value"))
                settings.setValue("value", DEFAULT_SETTINGS_SCREEN[key]);

            settings.endGroup();
        }
    }

    {
        auto existingKeys = settings.allKeys();

        for (auto key : DEFAULT_VALUES_INT.keys())
            if (!existingKeys.contains(key))
                settings.setValue(key, DEFAULT_VALUES_INT[key]);

        for (auto key : DEFAULT_VALUES_STRING.keys())
            if (!existingKeys.contains(key))
                settings.setValue(key, DEFAULT_VALUES_STRING[key]);

        for (auto key : DEFAULT_VALUES_BOOL.keys())
            if (!existingKeys.contains(key))
                settings.setValue(key, DEFAULT_VALUES_BOOL[key]);
    }
}

bool Utils::isPathWritable(QString path)
{
    QFileInfo fileInfo(path);
    return fileInfo.isWritable();
}
