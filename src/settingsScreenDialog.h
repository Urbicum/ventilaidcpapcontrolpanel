#ifndef SETTINGSSCREENDIALOG_H
#define SETTINGSSCREENDIALOG_H

#include <QDialog>

class BaseMessage;

namespace Ui {
class SettingsScreenDialog;
}

class SettingsScreenDialog : public QDialog {
    Q_OBJECT

public:
    explicit SettingsScreenDialog(QWidget* parent = nullptr);
    ~SettingsScreenDialog();
    void clear();

public slots:
    void onNewGenericMessage(QSharedPointer<const BaseMessage> msg);
    void onRequestNewMessage();

signals:
    void sendNewGenericMessage(QSharedPointer<const BaseMessage> msg);

private:
    Ui::SettingsScreenDialog* ui;
};

#endif // SETTINGSSCREENDIALOG_H
