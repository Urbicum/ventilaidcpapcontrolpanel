#ifndef RUNTIMECONFIG_H
#define RUNTIMECONFIG_H

#include <QString>

class CommandLineParser;

class RuntimeConfig {

public:
    RuntimeConfig(bool skipCrc = false, bool runHeadless = false);

    bool isSerialPortSet() const;
    bool skipCrc() const;
    QString getSerialPort() const;
    uint32_t getBaudRate() const;
    bool runHeadless() const;

private:
    uint32_t baudRate;
    QString serialPort;
    bool _skipCrc;
    bool _runHeadless;

    friend CommandLineParser;
};

#endif // RUNTIMECONFIG_H
