#include "secondaryScreenDialog.h"
#include "ui_secondaryScreenDialog.h"

SecondaryScreenDialog::SecondaryScreenDialog(QWidget* parent)
    : QDialog(parent)
    , ui(new Ui::SecondaryScreenDialog)
{
    ui->setupUi(this);

    ui->rotTarget->setup("Rot target:", "%1 L/m");
}

SecondaryScreenDialog::~SecondaryScreenDialog()
{
    delete ui;
}

void SecondaryScreenDialog::setRotTarget(float value)
{
    ui->rotTarget->setValue(value);
}

void SecondaryScreenDialog::clear()
{
    ui->rotTarget->clear();
}
