#include "commandLineParser.h"

CommandLineParser::CommandLineParser()
    : baudRate("baudRate",
          QCoreApplication::translate("main", "Sets baud rate."),
          QCoreApplication::translate("main", "baudRate"))
    , serialPort("serialPort",
          QCoreApplication::translate("main", "Sets serial port."),
          QCoreApplication::translate("main", "serialPortName"))
    , skipCrc("skipCrc", QCoreApplication::translate("main", "Flag to skip CRC calculation."))
    , runHeadless("headless", QCoreApplication::translate("main", "Run application without UI."))
{
    addVersionOption();
    addHelpOption();
    addOptions({ baudRate, serialPort, skipCrc, runHeadless });
}

RuntimeConfig CommandLineParser::validateAndGetRuntimeConfig()
{
    RuntimeConfig rtConfig;
    static const QList<uint> availableBaudRateValues = { 460800, 230400, 115200, 57600, 38400, 19200, 9600, 4800, 2400, 1200 };

    if (isSet(baudRate) && isSet(serialPort)) {
        bool ok = true;

        uint uiBaudRate = value(baudRate).toUInt(&ok);
        if (!ok)
            throw std::runtime_error("Baud rate has to be a positive integer");

        if (!availableBaudRateValues.contains(uiBaudRate))
            throw std::runtime_error("Baud rate has the incorrect value");

        rtConfig.baudRate = static_cast<uint32_t>(uiBaudRate);
        rtConfig.serialPort = value(serialPort);
    } else if (isSet(baudRate) || isSet(serialPort)) {
        throw std::runtime_error("Baud rate and serial port both have to be defined");
    }

    rtConfig._skipCrc = isSet(skipCrc);
    rtConfig._runHeadless = isSet(runHeadless);

    return rtConfig;
}
