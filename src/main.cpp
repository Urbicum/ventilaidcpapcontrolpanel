#include "commandLineParser.h"
#include "mainWindow.h"
#include "serialOverseer.h"
#include "utils.h"

#include <QApplication>
#include <QDebug>
#include <QSysInfo>

#ifndef Q_OS_WIN
#include <signal.h>
void catchUnixSignals(std::initializer_list<int> quitSignals)
{
    auto handler = [](int sig) -> void {
        printf("\nQuit the application by signal(%d).\n", sig);
        QCoreApplication::quit();
    };

    sigset_t blocking_mask;
    sigemptyset(&blocking_mask);
    for (auto sig : quitSignals)
        sigaddset(&blocking_mask, sig);

    struct sigaction sa;
    sa.sa_handler = handler;
    sa.sa_mask = blocking_mask;
    sa.sa_flags = 0;

    for (auto sig : quitSignals)
        sigaction(sig, &sa, nullptr);
}
#endif

RuntimeConfig parseCommandLineArgs(int argc, char* argv[])
{
    QCoreApplication a(argc, argv);

    CommandLineParser parser;
    parser.process(a);

    return parser.validateAndGetRuntimeConfig();
}

int main(int argc, char* argv[])
{
#ifndef Q_OS_WIN
    catchUnixSignals({ SIGINT, SIGTERM });
#endif

    RuntimeConfig config;

    {
        try {
            config = parseCommandLineArgs(argc, argv);
        } catch (std::runtime_error& e) {
            qDebug() << QString("Parsing command line arguments failed with: %1").arg(e.what());
            return 1;
        }
    }

    QSharedPointer<QCoreApplication> a;
    if (config.runHeadless())
        a = QSharedPointer<QCoreApplication>::create(argc, argv);
    else
        a = QSharedPointer<QApplication>::create(argc, argv);

    Utils::initApplication();
    Utils::initSettingsWithDefult();

    QString architecture = QSysInfo::currentCpuArchitecture();

    qDebug() << "Current CPU architecute:" << architecture;
    qDebug() << QString("Version: %1").arg(QApplication::applicationVersion());
    if (config.skipCrc())
        qWarning() << "Crc check will be skipped";

    Utils::Target target;
    if (architecture == "arm" || architecture == "arm64")
        target = Utils::Target::Raspberry;
    else
        target = Utils::Target::Desktop;

    SerialOverseer serialOverseer(nullptr, target, &config);

    QSharedPointer<MainWindow> w;
    if (config.runHeadless())
        qDebug() << "Running in headless mode";
    else {
        w = QSharedPointer<MainWindow>::create(nullptr, target, &serialOverseer);
        w->show();
    }

    if (config.isSerialPortSet())
        QTimer::singleShot(0, [&serialOverseer, &config]() {
            serialOverseer.open(config.getSerialPort(), static_cast<Serial::BaudRate>(config.getBaudRate()));
        });

    return a->exec();
}
