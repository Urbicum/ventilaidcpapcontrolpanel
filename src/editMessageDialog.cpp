#include "editMessageDialog.h"
#include "ui_editMessageDialog.h"

#include <QDataStream>
#include <QDebug>
#include <QDoubleSpinBox>
#include <QLabel>

EditMessageDialog::EditMessageDialog(QWidget* parent, QSharedPointer<const AnnotatedMessage> formatData)
    : QDialog(parent)
    , ui(new Ui::EditMessageDialog)
    , tmpMsg(formatData->clone())
{
    ui->setupUi(this);

    int row = 0;
    for (const auto it : *tmpMsg) {
        ui->gridLayout->addWidget(new QLabel(it.fieldName, this), row, 0);
        QAbstractSpinBox* newWidget = buildWidget(it);
        if (newWidget) {
            valueWidgets.push_back(newWidget);
            ui->gridLayout->addWidget(newWidget, row, 1);
        } else {
            qDebug() << "Couldn't create widget for row: " << row << " msgId: " << tmpMsg->getId();
        }
        row++;
    }
}

EditMessageDialog::~EditMessageDialog()
{
    delete ui;
}

QAbstractSpinBox* EditMessageDialog::buildWidget(const DataIterator::return_type& iterator)
{
    QAbstractSpinBox* widget = nullptr;
    switch (iterator.dataType) {
    case AnnotatedMessage::DataType::FLOAT:
        widget = new QDoubleSpinBox(this);
        dynamic_cast<QDoubleSpinBox*>(widget)->setValue(iterator.variantValue.value<float>());
        dynamic_cast<QDoubleSpinBox*>(widget)->setRange(std::numeric_limits<float>::lowest(), std::numeric_limits<float>::max());
        break;
    case AnnotatedMessage::DataType::UINT8_T:
        widget = new QSpinBox(this);
        dynamic_cast<QSpinBox*>(widget)->setValue(iterator.variantValue.value<uint8_t>());
        dynamic_cast<QSpinBox*>(widget)->setRange(std::numeric_limits<uint8_t>::lowest(), std::numeric_limits<uint8_t>::max());
        break;
    }
    return widget;
}

QSharedPointer<AnnotatedMessage> EditMessageDialog::getMessage() const
{
    QByteArray data;
    QDataStream out(&data, QIODevice::OpenModeFlag::WriteOnly);
    out.setFloatingPointPrecision(QDataStream::FloatingPointPrecision::SinglePrecision);
    out.setByteOrder(QDataStream::ByteOrder::LittleEndian);

    for (int idx = 0; idx < tmpMsg->getMemberNum(); idx++) {
        switch (tmpMsg->getMemberInfo()[idx].memberType) {
        case AnnotatedMessage::DataType::FLOAT:
            out << static_cast<float>(dynamic_cast<QDoubleSpinBox*>(valueWidgets[idx])->value());
            break;
        case AnnotatedMessage::DataType::UINT8_T:
            out << static_cast<uint8_t>(dynamic_cast<QSpinBox*>(valueWidgets[idx])->value());
            break;
        default:
            qDebug() << "Couldn't get value for idx: " << idx << " msgId: " << tmpMsg->getId();
        }
    }

    tmpMsg->replacePayload(std::move(data));
    return tmpMsg;
}
