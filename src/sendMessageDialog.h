#ifndef SENDMESSAGEDIALOG_H
#define SENDMESSAGEDIALOG_H

#include <QDialog>

#include <message/requestMessage.h>

namespace Ui {
class SendMessageDialog;
}

class SendRequestMessageDialog : public QDialog {
    Q_OBJECT

public:
    explicit SendRequestMessageDialog(QWidget* parent = nullptr);
    ~SendRequestMessageDialog();

    RequestMessage getMessage() const;

private:
    Ui::SendMessageDialog* ui;
};

#endif // SENDMESSAGEDIALOG_H
