#include "serial.h"

#include "message/alarmDetailsMessage.h"
#include "message/alarmStatusMessage.h"
#include "message/alarmThresholdMessage.h"
#include "message/breathAnalyserMessage.h"
#include "message/debugV2Message.h"
#include "message/flowCalibrationMessage.h"
#include "message/pidParamsMessage.h"
#include "message/pressureSensorCalibMessage.h"
#include "message/workpointCalibrationMessage.h"

#include <QSettings>

namespace {
static const QMap<uint8_t, uint8_t> VALID_MSG_SIZE = {
    { MainParamsMessage::MSG_ID, MainParamsMessage::SIZE },
    { MedicalParamsMessage::MSG_ID, MedicalParamsMessage::SIZE },
    { PIDParamsMessage::MSG_ID, PIDParamsMessage::SIZE },
    { FlowCalibrationMessage::MSG_ID, FlowCalibrationMessage::SIZE },
    { WorkpointCalibrationMessage::MSG_ID, WorkpointCalibrationMessage::SIZE },
    { BreathAnalyserMessage::MSG_ID, BreathAnalyserMessage::SIZE },
    { AlarmStatusMessage::MSG_ID, AlarmStatusMessage::SIZE },
    { AlarmDetailsMessage::MSG_ID, AlarmDetailsMessage::SIZE },
    { AlarmThresholdMessage::MSG_ID, AlarmThresholdMessage::SIZE },
    { PressureSensorCalibMessage::MSG_IDS[0], PressureSensorCalibMessage::SIZE },
    { PressureSensorCalibMessage::MSG_IDS[1], PressureSensorCalibMessage::SIZE },
    { PressureSensorCalibMessage::MSG_IDS[2], PressureSensorCalibMessage::SIZE },
    { PressureSensorCalibMessage::MSG_IDS[3], PressureSensorCalibMessage::SIZE },
    { DebugV2Message::MSG_ID, DebugV2Message::SIZE },
};
}

Serial::Serial(QObject* parent, bool skipCrc)
    : QObject(parent)
    , TIMEOUT_MS(QSettings().value("serialTimeoutMs").toUInt())
    , serialPort(nullptr)
    , watchdog(new QTimer(this))
    , _connected(false)
    , skipCrc(skipCrc)
{
    connect(watchdog, &QTimer::timeout, this, &Serial::handleTimeout);
}

Serial::~Serial()
{
    qDebug() << "~Serial";
}

bool Serial::isConnected() const
{
    return this->_connected;
}

void Serial::disconnect()
{
    close(DisconnectionReason::UserRequest, "");
}

void Serial::open(QString portName, Serial::BaudRate baudRate)
{
    serialPort = new QSerialPort(portName, this);
    bool baudRateOk = serialPort->setBaudRate(baudRate);

    if (!baudRateOk) {
        auto errorMsg = QString("Failed to open port: '%1' with baud rate: '%2' error: %3").arg(portName, QString::number(baudRate), serialPort->errorString());
        delete serialPort;
        serialPort = nullptr;

        emit disconnected(DisconnectionReason::Error, errorMsg);
    } else if (!serialPort->open(QIODevice::ReadWrite)) {
        auto errorMsg = QString("Failed to open port: '%1' With error: %2").arg(portName, serialPort->errorString());
        delete serialPort;
        serialPort = nullptr;

        emit disconnected(DisconnectionReason::Error, errorMsg);
    } else {
        connect(serialPort, &QSerialPort::readyRead, this, &Serial::handleReadyRead);
        connect(serialPort, &QSerialPort::errorOccurred, this, &Serial::handleError);

        _connected = true;
        watchdog->start(TIMEOUT_MS);
        emit connected(serialPort->portName());
    }
}

void Serial::onSendNewGenericMessage(QSharedPointer<const BaseMessage> msg)
{
    if (serialPort == nullptr) {
        qDebug() << "Error: Calling send message on closed serial";
        return;
    }

    auto medicalParamsgMsg = qSharedPointerDynamicCast<const MedicalParamsMessage>(msg);
    if (medicalParamsgMsg) {

        auto data = medicalParamsgMsg->getData();
        const qint64 bytesWrtitten = serialPort->write(data);

        if (bytesWrtitten != data.size()) {
            auto errorMsg = QString("Serial write failed with %1. Closing connection.").arg(serialPort->errorString());
            close(DisconnectionReason::Error, errorMsg);
        }
    }
}

void Serial::close(DisconnectionReason reason, QString message)
{
    qDebug() << QString("Closing serial, reason=%1 msg='%2'").arg(static_cast<int>(reason)).arg(message);

    watchdog->stop();
    // using `delete` instead of `deleteLater()` was causing crashes when manually disconecting serial cable
    serialPort->deleteLater();
    serialPort = nullptr;
    dataBuffer.resize(0);
    _connected = false;

    emit disconnected(reason, message);
}

void Serial::sendMessage(StatusMessage msg)
{
    if (serialPort == nullptr) {
        qDebug() << "Error: Calling send message on closed serial";
        return;
    }

    auto data = msg.getData();
    const qint64 bytesWrtitten = serialPort->write(data);

    if (bytesWrtitten != data.size()) {
        auto errorMsg = QString("Serial write failed with %1. Closing connection.").arg(serialPort->errorString());
        close(DisconnectionReason::Error, errorMsg);
    }
}

void Serial::sendMessage(RequestMessage msg)
{
    if (serialPort == nullptr) {
        qDebug() << "Error: Calling send message on closed serial";
        return;
    }

    auto data = msg.getData();
    const qint64 bytesWrtitten = serialPort->write(data);

    if (bytesWrtitten != data.size()) {
        auto errorMsg = QString("Serial write failed with %1. Closing connection.").arg(serialPort->errorString());
        close(DisconnectionReason::Error, errorMsg);
    }
}

void Serial::sendChangeParamsMessage(QSharedPointer<const AnnotatedMessage> msg)
{
    if (serialPort == nullptr) {
        qDebug() << "Error: Calling send message on closed serial";
        return;
    }

    const auto msgSize = VALID_MSG_SIZE.find(msg->getId());
    if (msgSize == VALID_MSG_SIZE.end()) {
        qDebug() << "Error: Calling send message with unknown msgId";
        return;
    }

    QByteArray data(*msgSize, 0);
    char* ptr = data.data();
    uint8_t payloadSize = msg->getPayload().size();

    // header
    *((uint8_t*)(ptr)) = BaseMessage::START_MARKER_ARRAY[0];
    *((uint8_t*)(ptr + 1)) = BaseMessage::START_MARKER_ARRAY[1];
    *((uint8_t*)(ptr + 2)) = msg->getSendId();
    *((uint8_t*)(ptr + 3)) = msg->getSize();

    // data
    {
        const uint8_t* payloadPtr = (uint8_t*)msg->getPayload().data();
        for (uint8_t payloadIdx = 0; payloadIdx < payloadSize; payloadIdx++, payloadPtr++) {
            *((uint8_t*)(ptr + BaseMessage::HEADER_SIZE + payloadIdx)) = *payloadPtr;
        }
    }

    // footer
    *((uint16_t*)(ptr + BaseMessage::HEADER_SIZE + payloadSize)) = qChecksum((char*)(ptr + BaseMessage::HEADER_SIZE), payloadSize, Qt::ChecksumType::ChecksumIso3309);
    *((uint8_t*)(ptr + BaseMessage::HEADER_SIZE + payloadSize + sizeof(uint16_t))) = BaseMessage::END_BYTE;

    const qint64 bytesWrtitten = serialPort->write(data);

    if (bytesWrtitten != data.size()) {
        auto errorMsg = QString("Serial write failed with %1. Closing connection.").arg(serialPort->errorString());
        close(DisconnectionReason::Error, errorMsg);
    }
}

void Serial::handleReadyRead()
{
    if (serialPort == nullptr) {
        qDebug() << "Error: Calling handle read on closed serial";
        return;
    }

    watchdog->start(TIMEOUT_MS);

    dataBuffer.append(serialPort->readAll());

    while (parseMessage())
        ;
}

// TODO: Move all of this out of Serial
bool Serial::parseMessage()
{
    auto idx = dataBuffer.indexOf(BaseMessage::START_MARKER_ARRAY);

    if (idx == -1) {
        // It is possible that part of the start marker is already at the end of dataBuffer
        // so we can't clear the entire dataBuffer.
        dataBuffer.remove(0, dataBuffer.size() - BaseMessage::START_MARKER_ARRAY.size());
        return false;
    } else
        dataBuffer.remove(0, idx);

    if (dataBuffer.size() < BaseMessage::HEADER_SIZE)
        return false; // we have to wait for more data in the buffer

    uint8_t* ptr = (uint8_t*)(dataBuffer.data());
    const uint8_t msgId = ptr[2];
    const uint8_t msgSize = ptr[3];

    if (dataBuffer.size() < msgSize)
        return false; // we have to wait for more data in the buffer

    uint8_t lastByte = *(ptr + msgSize - 1);
    if (lastByte != BaseMessage::END_BYTE) {
        qDebug() << "Discarding one byte, because of incorrect end marker: " << lastByte;

        // We know that dataBuffer starts with startMarker, so we remove one byte and try again
        dataBuffer.remove(0, 1);
        return true;
    }

    // check crc
    const uint8_t payloadSize = msgSize - BaseMessage::HEADER_SIZE - BaseMessage::FOOTER_SIZE;
    const uint16_t expectedCrc = *((uint16_t*)(ptr + msgSize - BaseMessage::FOOTER_SIZE));
    const uint16_t actualCrc = qChecksum((char*)(ptr + BaseMessage::HEADER_SIZE), payloadSize, Qt::ChecksumType::ChecksumIso3309);

    if (!skipCrc && expectedCrc != actualCrc) {
        qDebug() << QString("Discarding one byte, because of failed CRC check expected=%1 actual=%2").arg(expectedCrc).arg(actualCrc);

        // We know that dataBuffer starts with startMarker, so we remove one byte and try again
        dataBuffer.remove(0, 1);
        return true;
    }

    const bool bufferConsumed = parseMessageImpl(msgId, msgSize, payloadSize);
    if (!bufferConsumed) {
        qDebug() << "Discarding one byte, because of incorrect msgId: " << msgId;

        // We know that dataBuffer starts with startMarker, so we remove one byte and try again
        dataBuffer.remove(0, 1);
        return true;
    }

    return true; // keep parsing current dataBuffer
}

bool Serial::parseMessageImpl(uint8_t msgId, uint8_t msgSize, uint8_t payloadSize)
{
    if (sizeCheck(VALID_MSG_SIZE, msgId, msgSize))
        return true;

    auto removeAndEmitGenericMessage = [this](QSharedPointer<const BaseMessage> msg, uint8_t msgSize) {
        this->dataBuffer.remove(0, msgSize);
        emit newGenericMessage(msg);
    };

    auto removeAndEmitChangeParamsMessage = [this](QSharedPointer<const AnnotatedMessage> msg, uint8_t msgSize) {
        this->dataBuffer.remove(0, msgSize);
        emit newChangeParamsMessage(msg);
    };

    if (msgId == MainParamsMessage::MSG_ID) {
        auto msg = QSharedPointer<const MainParamsMessage>::create(dataBuffer.left(msgSize));
        removeAndEmitGenericMessage(msg, msgSize);
        return true;
    } else if (msgId == MedicalParamsMessage::MSG_ID) {
        auto msg = QSharedPointer<const MedicalParamsMessage>::create(dataBuffer.left(msgSize));
        removeAndEmitGenericMessage(msg, msgSize);
        return true;
    } else if (msgId == PIDParamsMessage::MSG_ID) {
        auto msg = QSharedPointer<const PIDParamsMessage>::create(dataBuffer.mid(BaseMessage::HEADER_SIZE, payloadSize));
        removeAndEmitChangeParamsMessage(msg, msgSize);
        return true;
    } else if (msgId == FlowCalibrationMessage::MSG_ID) {
        auto msg = QSharedPointer<const FlowCalibrationMessage>::create(dataBuffer.mid(BaseMessage::HEADER_SIZE, payloadSize));
        removeAndEmitChangeParamsMessage(msg, msgSize);
        return true;
    } else if (msgId == WorkpointCalibrationMessage::MSG_ID) {
        auto msg = QSharedPointer<const WorkpointCalibrationMessage>::create(dataBuffer.mid(BaseMessage::HEADER_SIZE, payloadSize));
        removeAndEmitChangeParamsMessage(msg, msgSize);
        return true;
    } else if (msgId == BreathAnalyserMessage::MSG_ID) {
        auto msg = QSharedPointer<const BreathAnalyserMessage>::create(dataBuffer.left(msgSize));
        removeAndEmitGenericMessage(msg, msgSize);
        return true;
    } else if (msgId == AlarmStatusMessage::MSG_ID) {
        auto msg = QSharedPointer<const AlarmStatusMessage>::create(dataBuffer.left(msgSize));
        removeAndEmitGenericMessage(msg, msgSize);
        return true;
    } else if (msgId == AlarmDetailsMessage::MSG_ID) {
        auto msg = QSharedPointer<const AlarmDetailsMessage>::create(dataBuffer.left(msgSize));
        removeAndEmitGenericMessage(msg, msgSize);
        return true;
    } else if (msgId == AlarmThresholdMessage::MSG_ID) {
        auto msg = QSharedPointer<const AlarmThresholdMessage>::create(dataBuffer.left(msgSize));
        removeAndEmitGenericMessage(msg, msgSize);
        return true;
    } else if (msgId == PressureSensorCalibMessage::MSG_IDS[0]) {
        auto msg = QSharedPointer<const PressureSensorCalibMessage>::create(dataBuffer.mid(BaseMessage::HEADER_SIZE, payloadSize), 0);
        removeAndEmitChangeParamsMessage(msg, msgSize);
        return true;
    } else if (msgId == PressureSensorCalibMessage::MSG_IDS[1]) {
        auto msg = QSharedPointer<const PressureSensorCalibMessage>::create(dataBuffer.mid(BaseMessage::HEADER_SIZE, payloadSize), 1);
        removeAndEmitChangeParamsMessage(msg, msgSize);
        return true;
    } else if (msgId == PressureSensorCalibMessage::MSG_IDS[2]) {
        auto msg = QSharedPointer<const PressureSensorCalibMessage>::create(dataBuffer.mid(BaseMessage::HEADER_SIZE, payloadSize), 2);
        removeAndEmitChangeParamsMessage(msg, msgSize);
        return true;
    } else if (msgId == PressureSensorCalibMessage::MSG_IDS[3]) {
        auto msg = QSharedPointer<const PressureSensorCalibMessage>::create(dataBuffer.mid(BaseMessage::HEADER_SIZE, payloadSize), 3);
        removeAndEmitChangeParamsMessage(msg, msgSize);
        return true;
    } else if (msgId == DebugV2Message::MSG_ID) {
        auto msg = QSharedPointer<const DebugV2Message>::create(dataBuffer.left(msgSize));
        removeAndEmitGenericMessage(msg, msgSize);
        return true;
    }

    return false;
}

void Serial::handleTimeout()
{
    auto errorMsg = QString("No data was recieved for %1ms. Closing connection.").arg(TIMEOUT_MS);
    close(DisconnectionReason::Error, errorMsg);
}

void Serial::handleError(QSerialPort::SerialPortError serialPortError)
{
    if (serialPortError == QSerialPort::NoError)
        return;

    auto errorMsg = QString("Serial port failed (error id=%1) with %2. Closing connection.").arg(QString::number(serialPortError), serialPort->errorString());
    close(DisconnectionReason::Error, errorMsg);
}

bool Serial::sizeCheck(const QMap<uint8_t, uint8_t>& expectedMessages, uint8_t msgId, uint8_t msgSize)
{
    const auto it = expectedMessages.find(msgId);
    if (it == expectedMessages.end() || it.value() != msgSize) {
        qDebug() << QString("Discarding one byte, because of incorrect msgSize=%1 for msgId=%2").arg(msgSize).arg(msgId);

        // We know that dataBuffer starts with startMarker, so we remove one byte and try again
        dataBuffer.remove(0, 1);
        return true;
    }

    return false;
}
