#include "sliderWidget.h"
#include "ui_sliderWidget.h"

#include <QSettings>

SliderWidget::SliderWidget(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::SliderWidget)
{
    ui->setupUi(this);
}

void SliderWidget::setup(float min, float max, QString name, QString unitTemplate, QString settingsKey)
{
    this->unitTemplate = unitTemplate;
    this->settingsKey = settingsKey;

    connect(ui->slider, &QSlider::valueChanged, [this](int value) {
        ui->setLabel->setText("Set: " + this->unitTemplate.arg(value));
    });

    ui->slider->setRange(min, max);
    ui->nameLabel->setText(name);

    QSettings settings;
    settings.beginGroup(settingsKey);

    ui->slider->setValue(settings.value("value").toInt());

    clear();
}

void SliderWidget::setValue(float val)
{
    ui->currentLabel->setText("Current: " + unitTemplate.arg(val));
}

float SliderWidget::getValue() const
{
    return ui->slider->value();
}

void SliderWidget::clear()
{
    ui->currentLabel->setText("Current: " + unitTemplate.arg("?"));
}

SliderWidget::~SliderWidget()
{
    QSettings settings;
    settings.beginGroup(settingsKey);
    settings.setValue("value", ui->slider->value());

    delete ui;
}
