#ifndef VALUEWIDGET_H
#define VALUEWIDGET_H

#include <QWidget>

namespace Ui {
class ValueWidget;
}

class ValueWidget : public QWidget {
    Q_OBJECT

public:
    explicit ValueWidget(QWidget* parent = nullptr);
    ~ValueWidget();

    void setup(QString name);
    void setup(QString name, QString unitTemplate);

    void setValue(QString value);
    void setValue(float value);

    void clear();

private:
    Ui::ValueWidget* ui;
    QString unitTemplate;
};

#endif // VALUEWIDGET_H
