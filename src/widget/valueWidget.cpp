#include "valueWidget.h"
#include "ui_valueWidget.h"

ValueWidget::ValueWidget(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::ValueWidget)
{
    ui->setupUi(this);
}

ValueWidget::~ValueWidget()
{
    delete ui;
}

void ValueWidget::setup(QString name)
{
    ui->name->setText(name);

    clear();
}

void ValueWidget::setup(QString name, QString unitTemplate)
{
    ui->name->setText(name);
    this->unitTemplate = unitTemplate;

    clear();
}

void ValueWidget::setValue(QString value)
{
    ui->value->setText(value);
}

void ValueWidget::setValue(float value)
{
    ui->value->setText(unitTemplate.arg(value, 4, 'f', 1));
}

void ValueWidget::clear()
{
    if (unitTemplate.size() == 0)
        ui->value->setText("?");
    else
        ui->value->setText(unitTemplate.arg("?"));
}
