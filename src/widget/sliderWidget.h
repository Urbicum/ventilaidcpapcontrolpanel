#ifndef SLIDERWIDGET_H
#define SLIDERWIDGET_H

#include <QWidget>

namespace Ui {
class SliderWidget;
}

class SliderWidget : public QWidget {
    Q_OBJECT

public:
    explicit SliderWidget(QWidget* parent = nullptr);
    ~SliderWidget();

    void setup(float min, float max, QString name, QString unitTemplate, QString settingsKey);
    void setValue(float val);
    float getValue() const;
    void clear();

private:
    Ui::SliderWidget* ui;
    QString unitTemplate;
    QString settingsKey;
};

#endif // SLIDERWIDGET_H
